/*
SQLyog Ultimate
MySQL - 8.0.11 : Database - loanmarket_develop_sources
*********************************************************************
*/

USE `loanmarket_develop`;

/*Table structure for table `cu_flash` */

CREATE TABLE `cu_flash` (
  `id` int(11) NOT NULL,
  `itemId` int(11) DEFAULT NULL COMMENT 'cu_Item的ID',
  `photo` varchar(128) DEFAULT NULL COMMENT '图片',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `onOff` varchar(32) DEFAULT NULL COMMENT '是否禁用',
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cu_flash` */

insert  into `cu_flash`(`id`,`itemId`,`photo`,`sort`,`onOff`,`note`) values 
(1,54,'flash\\photo_1_20190704090459041.jpg',0,'on',NULL),
(2,53,'flash\\photo_2_20190704090541965.jpg',2,'on',NULL);

/*Table structure for table `cu_group` */

CREATE TABLE `cu_group` (
  `id` int(11) NOT NULL,
  `Name` varchar(64) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `onOff` varchar(32) DEFAULT NULL COMMENT '是否禁用',
  `note` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cu_group` */

insert  into `cu_group`(`id`,`Name`,`sort`,`onOff`,`note`) values 
(0,'客户分组',0,'on',NULL),
(1,'热门推荐',0,'on',NULL),
(2,'新口子',0,'on',NULL),
(3,'速贷',0,'on','');

/*Table structure for table `cu_group_item_asso` */

CREATE TABLE `cu_group_item_asso` (
  `id` int(11) NOT NULL,
  `gid` int(11) DEFAULT NULL COMMENT 'group_id',
  `iid` int(11) DEFAULT NULL COMMENT 'item_id',
  `sort` int(11) DEFAULT NULL COMMENT '排序，只能填写int',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`gid`,`iid`) COMMENT '两个并列唯一键'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cu_group_item_asso` */

insert  into `cu_group_item_asso`(`id`,`gid`,`iid`,`sort`) values 
(30,1,23,3),
(70,1,44,2),
(87,3,44,NULL),
(92,1,50,1),
(93,4,50,NULL),
(94,1,52,3),
(95,2,52,3),
(96,1,53,3),
(97,2,53,1),
(98,1,54,4),
(99,2,54,NULL),
(100,2,51,NULL),
(101,2,50,NULL);

/*Table structure for table `cu_item` */

CREATE TABLE `cu_item` (
  `id` int(11) NOT NULL,
  `Name` varchar(64) DEFAULT NULL,
  `logo` varchar(128) DEFAULT NULL COMMENT '图片',
  `description` varchar(128) DEFAULT NULL COMMENT '一句话描述',
  `MaxLimit` varchar(128) DEFAULT NULL COMMENT '最大额度',
  `succRate` varchar(32) DEFAULT NULL COMMENT '成功率',
  `quota` varchar(32) DEFAULT NULL COMMENT '今日名额（已抢）',
  `appNum` int(11) DEFAULT NULL COMMENT '申请数（非统计数据，只作显示）',
  `deadline` varchar(128) DEFAULT NULL COMMENT '截止时间',
  `receiveTime` varchar(128) DEFAULT NULL COMMENT '最快放款时间',
  `loanTime` varchar(128) DEFAULT NULL COMMENT '可借用时间',
  `rate` varchar(32) DEFAULT NULL COMMENT '费率',
  `url` varchar(256) DEFAULT NULL COMMENT '推广链接',
  `onOff` varchar(32) DEFAULT NULL COMMENT '是否禁用',
  `note` varchar(255) DEFAULT NULL,
  `slogan1` varchar(255) DEFAULT NULL COMMENT '广告语1',
  `slogan2` varchar(255) DEFAULT NULL COMMENT '广告语2',
  `pv` int(11) DEFAULT NULL COMMENT '不统计当天',
  `uv` int(11) DEFAULT NULL COMMENT '不统计当天',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cu_item` */

insert  into `cu_item`(`id`,`Name`,`logo`,`description`,`MaxLimit`,`succRate`,`quota`,`appNum`,`deadline`,`receiveTime`,`loanTime`,`rate`,`url`,`onOff`,`note`,`slogan1`,`slogan2`,`pv`,`uv`) values 
(3,'米当家','logo/logo_3_20181021162215547.jpg','零门槛，1000-5000额度三分钟放款。','5000元',NULL,NULL,NULL,'7天',NULL,NULL,'0.03%','https://mdj.yinduoduo2018.com/mdj/regist/a5b1c7023ad5452eaf1d360f8ea1c0d1','on',NULL,NULL,NULL,NULL,NULL),
(4,'小猪快借','temp/1539863929(1).jpg','芝麻分580，秒到账','2000元',NULL,NULL,NULL,'7-14天',NULL,'14天','0.2%','https://www.newkouzi.com/app/wk51','on',NULL,NULL,NULL,NULL,NULL),
(6,'小猪快借','temp/1539863929(1).jpg','芝麻分580，秒到账','2000元',NULL,NULL,NULL,'7-14天',NULL,'14天','0.2%','https://www.newkouzi.com/app/wk51','off',NULL,NULL,NULL,NULL,NULL),
(7,'小白来花','logo/logo_7_20181107103636604.jpg','24小时全线上审核，快至1分钟极速到账','10000元',NULL,NULL,NULL,'7天-14天',NULL,'14天','0.03%','https://jsqb.kdqugou.com/newh5/web/page/landing-page?id=10&source_tag=61137','on','大白钱包是一款为年轻人提供资金周转的借贷平台，拥有60s极速放款，仅需身份证即可轻松申请，超高信用额度等独特优势。',NULL,NULL,1171,1002),
(8,'多万钱包','logo/logo_8_20181022173849676.png','下款快，额度高，无抵押信用贷款','1000元',NULL,NULL,NULL,'7-14天',NULL,NULL,'0.09%','http://sj.jieduoduo.tengjin.com/lexiangdai?channel=QQFGXX60','off',NULL,NULL,NULL,39,27),
(9,'铂金钱包','logo/logo_9_20181022173920510.png','国内领先的极速贷款平台','10000',NULL,NULL,NULL,'7天',NULL,'7天','0.065%','http://platinum.minfujr.cn/h5/register/register_hmg.html?invent_id=&channel_id=houshu20','off','20-45周岁,小额、快速贷、身份证即可.',NULL,NULL,69,54),
(11,'钱包到','logo/logo_11_20181026112339297.png','600分以上，自动审核，可续贷。','50000',NULL,NULL,NULL,'30天-90天',NULL,'90天','0.01%','https://sensors-api.kongapi.com/t/LB','on','18岁以上，手机实名6个月，不看征信，600分秒过',NULL,NULL,798,599),
(16,'随心花','logo/logo_16_20181106183519933.png','仅凭身份证，快速下款','4000',NULL,NULL,NULL,'7天-30天',NULL,NULL,'0.1%','https://h5.cangnanmingwei.com/sxhRegister-ChannelBak.html?QD1138','off',NULL,NULL,NULL,124,112),
(17,'喜贷','logo/logo_17_20181026180540807.png','9点-19点，15分钟极速审核，年龄：22-40','20000',NULL,NULL,NULL,'7-15天',NULL,NULL,'0.55%','https://xidaiqianbao.tjdzjq.com/mobile/phoneverification?par=69DC2BA9A7C6EB2B38D321EDBC10EED9','off',NULL,NULL,NULL,294,255),
(18,'瓜子口袋','logo/logo_18_20181024184332935.png','通过率高，到账迅速，提额快，24小时随时借。','10000',NULL,NULL,NULL,'7天',NULL,NULL,'3.57%','http://h5.whhxlc.com/guazi?source_tag=64','off',NULL,NULL,NULL,14,12),
(19,'有贝','logo/logo_19_20181025104614301.png','10分钟审核 急速放款','10000',NULL,NULL,NULL,'7天',NULL,NULL,'0.67%','https://toupin.cn/html/youbei/reg.html?channel=dsj91','off',NULL,NULL,NULL,129,117),
(21,'嗨购','logo/logo_21_20181031172203636.png','平台回收，瞬间转卖，秒下款（芝麻分550以上）','5000',NULL,NULL,NULL,'7天',NULL,NULL,'0.04%','http://www.51haigoo.com/h5/h5Page/51channel.html?h5PageTypeCode=21&channelId=b42ab5eefc6f4c68a982213a533492d8','on',NULL,NULL,NULL,214,161),
(22,'七天钱包','logo/logo_22_20181026103915289.png','19-40周岁；芝麻分580','10000',NULL,NULL,NULL,'7天',NULL,NULL,'0.03%','https://qtqbje.betterying.com/?mu31','off',NULL,NULL,NULL,7,7),
(23,'小马钱包','logo/logo_23_20181026144956823.jpg','有身份证就可借5000，3分钟放款！','5000',NULL,NULL,NULL,'7天',NULL,NULL,'0.03%','http://xmqbapp.sudaiboshi.com/h5/web/register/landing-page3.html?source_tag=35','on',NULL,NULL,NULL,251,224),
(24,'壹钱包','logo/logo_24_20181106195101906.png','线上申请，不看黑白，当天下款，超时必赔','1000',NULL,NULL,NULL,'7天',NULL,NULL,'0.03%','https://yqb.iguophone.com/?channel=djqb&utm_source=djqb&utm_medium=djqb#/index','off',NULL,NULL,NULL,290,263),
(25,'易秒下款','logo/logo_25_20181031133722885.png','十分钟极速下款','5000',NULL,NULL,NULL,'7天',NULL,'7天','3.57%','https://h5.yimenghd.com/register/yimiaojie.html?origin=2665','on',NULL,NULL,NULL,126,115),
(26,'满意袋','logo/logo_26_20181031171649853.jpg','凭身份证借款秒到账','5000',NULL,NULL,NULL,'7天',NULL,NULL,'0.03%','http://www.weiyingxiao.com/adinfo.aspx?isad=1&otype=1&adoid=ZMyEQTcRZ1VYEY1vqqCmbQ==','on',NULL,NULL,NULL,NULL,NULL),
(33,'抖贝','logo/logo_33_20181107153848370.png','三步审核，申请6千只要2分钟','6000',NULL,NULL,NULL,'1-9个月',NULL,NULL,'0.027%','http://h.yuchanginf.cn/wap/index.php?ctl=loan_template_three&partner=djqz','on',NULL,NULL,NULL,118,92),
(34,'小带鱼','logo/logo_34_20181107222118035.png','被拒了6次，终于在这儿借到钱了!!!','8000',NULL,NULL,NULL,'7天，14天，30天，90天',NULL,NULL,'0.027%','http://lp.xdy7.com/?tcid=461','on',NULL,NULL,NULL,140,122),
(36,'快借360','logo/logo_36_20181108175015858.jpg','急钱用不求人，20000额度任你花！','5000',NULL,NULL,NULL,'7天~12个月',NULL,NULL,'0.03%','https://kj360.cjzc168.com/384127','on',NULL,NULL,NULL,4,4),
(38,'微贷','logo/logo_38_20190401150018689.jpg','多米贷是微贷网推出的一款全新的无抵押、无担保的信用贷产品','20000元',NULL,NULL,NULL,'6-12期',NULL,NULL,'0.6%','https://static1.weidai.com.cn/static/clapp/2018/09/duomiLoan/prom4New.html?channel=DWHAAEE-001','on',NULL,NULL,NULL,13,13),
(39,'久久贷','logo/logo_39_20190408113217257.png','闪电秒批  新时贷','8000',NULL,NULL,NULL,'0-3个月',NULL,NULL,'0.03%','http://dxw.so/mlMm1F','on',NULL,NULL,NULL,1,1),
(40,'借点花','logo/logo_40_20190418162945624.png','无视征信，凭身份证，即可下款','50000',NULL,NULL,NULL,'30天',NULL,'24月','0.03','http://tudou.jienihua100.com/static/share/index.html?origin=hao','on',NULL,NULL,NULL,4,4),
(41,'信用贷','logo/logo_41_20190411115642140.png','一分钟下款 期期提额','20000',NULL,NULL,NULL,'0-31天',NULL,NULL,'日利率0.005%','https://channel-ddq.2orangetech.com/#/wdx/wdxregister?source=wdx_houshu_001','on',NULL,NULL,NULL,3,3),
(42,'高兴用','logo/logo_42_20190412181240193.png','急用钱，找我们','1500',NULL,NULL,NULL,'0-14天',NULL,NULL,'0.03%','http://pop.gaoxingyong.com/h5/register.html?7CC1B7C825','on',NULL,NULL,NULL,1,1),
(43,'金享汇','logo/logo_43_20190412181538846.png','全自动审核','30000',NULL,NULL,NULL,'0-12月',NULL,NULL,'日利率0.03','http://m.jinxianghui.net/?source=113','on',NULL,NULL,NULL,1,1),
(44,'蚂蚁闪电借','logo/logo_44_20190418162920871.png','随还随借，续借提额！','30000',NULL,NULL,NULL,'30天',NULL,NULL,'日息0.05%','https://taobei.pinganedai.vip/htmls/h5_mysdj/index.html?fc=djqz02','on',NULL,NULL,NULL,19,16),
(45,'自助钱包','logo/logo_45_20190415160549291.png','秒审，15分钟放款','50000',NULL,NULL,NULL,'15-30天',NULL,NULL,'日0.003','http://www.tianhuijr.com/user_register?admin_id=48&umstat=djqz02','on',NULL,NULL,NULL,5,5),
(46,'虎虎贷','logo/logo_46_20190418162841663.png','纯线上审核，极速到账','3000',NULL,NULL,NULL,'30天',NULL,NULL,'日息0.02','https://tts.kuaizu99.cn/static/download.html?show=1&agentid=124008','on',NULL,NULL,NULL,12,12),
(47,'一口贷','logo/logo_47_20190418161723529.png','在线审批，随还随借','8000',NULL,NULL,NULL,'30天',NULL,NULL,'日0.002','http://www.zhangshandai.com/h5/index.html?_p=com.yikoudai.yyios&_c=dongjia&_s=29&address=3','on',NULL,NULL,NULL,9,7),
(48,'即用钱','logo/logo_48_20190418163537843.png','申请拿钱，即刻到账','3000',NULL,NULL,NULL,'30天',NULL,'30天','0.002','https://m.shzying.com/#/auth/register?ref=jyqdaichao43','on',NULL,NULL,NULL,1,1),
(49,'江湖急需','logo/logo_49_20190419114348543.png','申请方便 纯信用贷 放款快！','5000',NULL,NULL,NULL,'15天',NULL,NULL,'0.01%','http://jhjxshare.51zd.top/extend/index?source=xinxing2','on',NULL,NULL,NULL,50,14),
(50,'芒果借','logo/logo_50_20190505151222331.png','借款简单，极速到账。','2000',NULL,NULL,NULL,'6天',NULL,NULL,'0','http://app.mangguojie123.com/cr/t9d91','on',NULL,NULL,NULL,22,6),
(51,'至上钱包','logo/logo_51_20190508111024802.png','简单操作，快速拿钱，最快15秒到账','10000',NULL,NULL,NULL,'7天',NULL,NULL,'0.05%','http://47.99.228.6:8008/channel/login2.html?channel=ltam2','on',NULL,NULL,NULL,2,2),
(52,'财米花','logo/logo_52_20190515094536340.png','自动审核秒到账','5000',NULL,NULL,NULL,'16天',NULL,'7','0.003','http://app.caimihua.com/cr/ZWySN','on',NULL,NULL,NULL,1,1),
(53,'好人钱包','logo/logo_53_20190516090315788.png','贷款最快一天到账，审批流程简单','8000',NULL,NULL,NULL,'15',NULL,'7','0.0039','http://hrqbzc.alifenqi.com/mobile/public/register?channel=djqz','on',NULL,NULL,NULL,NULL,NULL),
(54,'超E借','logo\\logo_54_20190704090746061.gif','当天放款，更快拿钱，凭实名手机立借30000元','5000','98%','12',45124,'2019-05-22','3分钟','120天','0.001','http://47.103.74.212:8080/h5/invite.jsp?channelCode=xdg333','on',NULL,'我想我应该不会喜欢你','我喜欢你，是我独家的记忆',NULL,NULL);

/*Table structure for table `cu_item_today` */

CREATE TABLE `cu_item_today` (
  `id` int(11) NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `current` varchar(32) DEFAULT NULL COMMENT '当选显示标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cu_item_today` */

insert  into `cu_item_today`(`id`,`itemId`,`sort`,`current`) values 
(1,34,1,''),
(2,54,0,''),
(3,53,0,''),
(5,51,0,''),
(6,50,5,''),
(7,52,0,''),
(8,49,0,''),
(9,48,0,''),
(10,45,0,''),
(11,47,0,''),
(12,46,0,''),
(13,44,0,''),
(15,51,8,'');

/*Table structure for table `cu_item_today_config` */

CREATE TABLE `cu_item_today_config` (
  `id` int(11) NOT NULL,
  `timeCount` int(32) DEFAULT NULL COMMENT '时间，数字,根据changway为单位',
  `timeUnit` varchar(32) DEFAULT NULL COMMENT '时间单位（day,hour）',
  `sortWay` varchar(32) DEFAULT NULL COMMENT '排序方式，pv,uv，排序（id，sort）',
  `ascOrDesc` varchar(32) DEFAULT NULL COMMENT '正序，倒序',
  `onOff` varchar(32) DEFAULT NULL COMMENT 'on:开启，off:关闭',
  `md5` varchar(128) DEFAULT NULL COMMENT '判断配置是否被改动',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cu_item_today_config` */

insert  into `cu_item_today_config`(`id`,`timeCount`,`timeUnit`,`sortWay`,`ascOrDesc`,`onOff`,`md5`) values 
(1,1,'hour','pv','desc','on','38375b5203de9e378bd0e59716c3d5f8');

/*Table structure for table `plugin_airing` */

CREATE TABLE `plugin_airing` (
  `id` int(8) NOT NULL,
  `text` varchar(1025) DEFAULT NULL,
  `var1` varchar(32) DEFAULT NULL,
  `var2` varchar(32) DEFAULT NULL,
  `var3` varchar(32) DEFAULT NULL,
  `showText` varchar(1025) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `plugin_airing` */

insert  into `plugin_airing`(`id`,`text`,`var1`,`var2`,`var3`,`showText`) values 
(1,'已有${var1}人申请借款，最快3分钟放款','791384',NULL,NULL,'已有791384人申请借款，最快3分钟放款'),
(2,NULL,NULL,NULL,NULL,'http://data.softmarket.cn/G.ashx?id=pwyVsP');

/*Table structure for table `sys_button` */

CREATE TABLE `sys_button` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `code` varchar(64) DEFAULT NULL COMMENT '编码生成规则：B + menu_id + button_id\r\n            例如：菜单ID：12；按钮ID：23，则按钮编码：B1223\r\n            ',
  `isHidden` char(1) DEFAULT '0',
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

/*Data for the table `sys_button` */

insert  into `sys_button`(`id`,`menu_id`,`name`,`code`,`isHidden`,`note`) values 
(5,24,'顶顶顶','M24B5','1',NULL),
(6,29,'添加','M29B6_add','0',NULL),
(7,29,'编辑','M29B7_edit','0',NULL),
(8,29,'删除','M29B8_del','0',NULL),
(9,29,'禁用','M29B9_dis','0',NULL),
(10,30,'添加','M30B10_add','0',NULL),
(11,30,'编辑','M30B11_edit','0',NULL),
(12,30,'删除','M30B12_del','0',NULL),
(13,31,'编辑','M31B13_edit','0',NULL),
(14,32,'添加','M32B14_add','0',NULL),
(15,32,'编辑','M32B15_edit','0',NULL),
(16,32,'删除','M32B16_del','0',NULL),
(17,32,'改密','M32B17_pwd','0',NULL),
(18,32,'禁用','M32B18_dis','0',NULL),
(19,33,'编辑_菜单','M33B19_edit','0',NULL),
(20,33,'编辑_按钮','M33B20_edit','0',NULL),
(21,36,'添加','M36B21_add','0',NULL),
(22,36,'删除','M36B22_del','0',NULL),
(23,36,'编辑','M36B23_edit','0',NULL),
(24,36,'禁用','M36B24_dis','0',NULL),
(49,40,'添加','M40B49_add','0',NULL),
(50,40,'删除','M40B50_del','0',NULL),
(51,40,'编辑','M40B51_edit','0',NULL),
(56,53,'新建渠道','M53B56_add','0',NULL),
(57,38,'添加分组','M38B57_add','0',NULL),
(58,38,'禁用','M38B58_dis','0',NULL),
(59,38,'编辑分组','M38B59_edit','0',NULL),
(60,38,'删除分组','M38B60_del','0',NULL),
(61,39,'添加产品','M39B61_add','0',NULL),
(62,39,'编辑产品','M39B62_edit','0',NULL),
(63,39,'禁用','M39B63_dis','0',NULL),
(64,39,'删除产品','M39B64_del','0',NULL),
(65,44,'添加产品','M44B65_add','0',NULL),
(66,44,'禁用','M44B66_dis','0',NULL),
(67,44,'编辑','M44B67_edit','0',NULL),
(68,44,'删除','M44B68_del','0',NULL),
(69,45,'添加','M45B69_add','0',NULL),
(70,45,'预览','M45B70_view','0',NULL),
(71,45,'禁用','M45B71_dis','0',NULL),
(72,45,'删除','M45B72_del','0',NULL),
(73,45,'编辑','M45B73_edit','0',NULL),
(74,44,'预览','M44B74_view','0',NULL),
(75,53,'修改渠道','M53B75_edit','0',NULL),
(76,49,'添加图片','M49B76_add','0',NULL),
(77,49,'预览幻灯','M49B77_view','0',NULL),
(78,49,'禁用','M49B78_dis','0',NULL),
(79,49,'修改幻灯','M49B79_edit','0',NULL),
(80,49,'删除幻灯','M49B80_del','0',NULL),
(81,50,'选择已有产品','M50B81_add','0',NULL),
(82,50,'修改排序','M50B82_edit','0',NULL),
(83,50,'删除关联','M50B83_del','0',NULL),
(84,47,'新建分组','M47B84_add','0',NULL),
(85,47,'禁用','M47B85_dis','0',NULL),
(86,47,'修改','M47B86_edit','0',NULL),
(87,47,'删除','M47B87_del','0',NULL);

/*Table structure for table `sys_dict` */

CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Pcode` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `code` varchar(64) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

/*Data for the table `sys_dict` */

insert  into `sys_dict`(`id`,`Pcode`,`name`,`code`,`note`) values 
(1,'0','角色状态','roleStatus',NULL),
(2,'roleStatus','启用','on',NULL),
(3,'roleStatus','禁用','off',NULL),
(7,'0','用户状态','userStatus',NULL),
(8,'userStatus','启用','on',NULL),
(9,'userStatus','禁用','off',NULL),
(10,'0','按钮','button','配置按钮，千万别乱动'),
(11,'button','预览','view','view'),
(12,'button','添加','add','add'),
(13,'button','编辑','edit','edit'),
(14,'button','删除','del','delete'),
(15,'button','禁用','dis','disable'),
(16,'button','下载','down','download'),
(17,'button','上传','up','upload'),
(18,'button','改密','pwd','password'),
(19,'button','踢除','kick','Kicking'),
(47,'0','是否','yesOrno',NULL),
(48,'yesOrno','是','yes',''),
(49,'yesOrno','否','no',''),
(61,'button','子菜单','subList','');

/*Table structure for table `sys_menu` */

CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Pid` int(11) DEFAULT '0',
  `code` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `isHidden` char(1) DEFAULT '0',
  `icon` varchar(128) DEFAULT NULL,
  `sort` varchar(5) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`id`,`Pid`,`code`,`name`,`url`,`isHidden`,`icon`,`sort`,`note`) values 
(0,-1,'root','根菜单',NULL,'0',NULL,NULL,NULL),
(28,0,'sysManage','系统管理',NULL,'0','','7',NULL),
(29,28,'role','角色管理','/admin/roleList.html','0','&#xe62d;',NULL,NULL),
(30,28,'cloumn','栏目管理','/admin/systemCategory.html','0','&#xe681;',NULL,NULL),
(31,28,'sys_dict','数据字典','/admin/dictList.html','0','',NULL,NULL),
(32,28,'userM','用户管理','/admin/userList.html','0','&#xe60d;','0','111'),
(33,28,'roleAuth','角色权限','/admin/roleAuth.html','0','&#xe70d;',NULL,NULL),
(36,28,'button','按钮配置','/admin/buttonList.html','0','&#xe63c;',NULL,NULL),
(37,0,'loanMarket','贷款产品',NULL,'0','','1',NULL),
(39,37,'groupItem','产品分组','/popluarize/cugroupList/groupItem','0','&#xe681;','1',NULL),
(44,37,'allProduct','所有产品','/popluarize/cuitemList','0','&#xe613;','0',NULL),
(46,0,'loanType','贷款分类',NULL,'0','','0',NULL),
(47,46,'loanClass','分类管理','/popluarize/cugroupList','0','&#xe64b;',NULL,NULL),
(48,0,'Flash','轮播图',NULL,'0','','2',NULL),
(49,48,'homeFlash','首页轮播','/popluarize/cuflashList','0','&#xe646;',NULL,'0'),
(50,37,'recommend','今日推荐','/popluarize/cuitemtodayList','0','&#xe6bb;','2',NULL),
(59,0,'system Info','系统信息',NULL,'0','&#xe72d;','6',NULL),
(60,59,'druidMonitor','Druid Monitor','/druid/index.html','0','&#xe6b5;','0',NULL);

/*Table structure for table `sys_role` */

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `code` varchar(64) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `note` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`,`code`,`status`,`note`) values 
(1,'超级管理员','superadmin','on','ffff11'),
(3,'普通用户','user','on',NULL);

/*Table structure for table `sys_role_menu` */

CREATE TABLE `sys_role_menu` (
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  UNIQUE KEY `AK_Key_1` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`role_id`,`menu_id`) values 
(1,0),
(1,28),
(1,29),
(1,30),
(1,31),
(1,32),
(1,33),
(1,36),
(1,37),
(1,39),
(1,44),
(1,46),
(1,47),
(1,48),
(1,49),
(1,50),
(1,59),
(1,60),
(3,0),
(3,37),
(3,39),
(3,44);

/*Table structure for table `sys_role_menu_button` */

CREATE TABLE `sys_role_menu_button` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `button_id` int(11) NOT NULL,
  UNIQUE KEY `AK_Key_1` (`role_id`,`menu_id`,`button_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_menu_button` */

insert  into `sys_role_menu_button`(`role_id`,`menu_id`,`button_id`) values 
(1,38,34),
(1,38,35),
(1,38,36),
(1,38,37),
(1,38,38),
(1,38,57),
(1,38,58),
(1,38,59),
(1,38,60),
(1,39,39),
(1,39,40),
(1,39,41),
(1,39,42),
(1,39,61),
(1,39,62),
(1,39,63),
(1,39,64),
(1,44,65),
(1,44,66),
(1,44,67),
(1,44,68),
(1,44,74),
(1,45,69),
(1,45,70),
(1,45,71),
(1,45,72),
(1,45,73),
(1,47,84),
(1,47,85),
(1,47,86),
(1,47,87),
(1,49,76),
(1,49,77),
(1,49,78),
(1,49,79),
(1,49,80),
(1,50,81),
(1,50,82),
(1,50,83),
(1,53,75),
(3,38,34),
(3,38,40),
(3,38,41),
(3,38,47),
(3,39,43),
(3,39,44),
(3,39,46),
(3,39,55),
(3,39,61),
(3,39,62),
(3,39,63),
(3,39,64),
(3,40,49),
(3,40,50),
(3,40,51),
(3,44,65),
(3,44,66),
(3,44,67),
(3,44,68),
(3,45,69),
(3,45,70),
(3,45,71),
(3,45,72),
(3,45,73);

/*Table structure for table `sys_user` */

CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(64) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `plaintext` varchar(128) DEFAULT NULL,
  `createTime` varchar(64) DEFAULT NULL,
  `status` varchar(28) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`nick`,`username`,`password`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`nick`,`username`,`password`,`plaintext`,`createTime`,`status`,`note`) values 
(1,'超级管理员','superadmin','$shiro1$SHA-256$500000$a/K3p1Mfd5fqjIEyLhMPZQ==$coTCpp+Ks+wO4ouWnWS+Gukkl26U52oUGI5AWk0ugKk=','','2018-06-27 21:12:30','on','12356456'),
(3,'哈哈哈','zzyi','$shiro1$SHA-256$500000$ln4vfhuPlXXKL0kX9qqWyw==$G8O/fSTOLY94Q4QilCBJzTlE4n/J/i8Ywmv/U2nglAE=','123456','2019-04-10 17:29:16','on',NULL);

/*Table structure for table `sys_user_role` */

CREATE TABLE `sys_user_role` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  UNIQUE KEY `AK_Key_1` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`user_id`,`role_id`) values 
(1,1),
(3,3);

/*Table structure for table `tel_customer` */

CREATE TABLE `tel_customer` (
  `id` int(11) NOT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `nickName` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `province` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `carrier` varchar(128) DEFAULT NULL,
  `type` varchar(128) DEFAULT NULL,
  `registerTime` varchar(32) DEFAULT NULL COMMENT '第一次登陆时间',
  `userLogo` varchar(255) DEFAULT NULL COMMENT '用户logo地址',
  `sessionId` varchar(64) DEFAULT NULL COMMENT '登陆唯一标识',
  `lastLoginTime` varchar(32) DEFAULT NULL COMMENT '上次登录时间',
  `source` varchar(128) DEFAULT NULL COMMENT '注册来源（渠道）',
  `hidden` varchar(32) DEFAULT NULL COMMENT '是否扣量（yes,no）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `telephone` (`telephone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tel_customer_collection` (
  `id` int(11) NOT NULL,
  `cuId` int(11) DEFAULT NULL COMMENT 'tel_customer的id，即用户的id',
  `itemId` int(11) DEFAULT NULL COMMENT 'cu_item的id，产品的ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tel_customer_collection` */

insert  into `tel_customer_collection`(`id`,`cuId`,`itemId`) values 
(1,2,23),
(3,2,49),
(4,2,46),
(5,1,23),
(6,1,44),
(7,19,46),
(8,20,44);

/*Table structure for table `tel_customer_loginhistory` */

CREATE TABLE `tel_customer_loginhistory` (
  `id` int(11) NOT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `smsCode` varchar(128) DEFAULT NULL,
  `sendCodeTime` varchar(32) DEFAULT NULL,
  `loginTime` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tel_customer_loginhistory` */

insert  into `tel_customer_loginhistory`(`id`,`telephone`,`smsCode`,`sendCodeTime`,`loginTime`) values 
(1,'18621910240','【我的钱袋】您的验证码是：623458，打死都不能告诉别人。','2019-03-25 11:40:01',NULL),
(2,'18621910240','【xxxxx】您的验证码是：400261，打死都不能告诉别人。','2019-03-25 11:53:55',NULL),
(3,'18621910240','【123456】您的验证码是：924432，打死都不能告诉别人。','2019-03-25 11:55:17',NULL),
(4,'18621910240','【需要签名】您的验证码是：571338，打死都不能告诉别人。','2019-03-25 11:55:47',NULL),