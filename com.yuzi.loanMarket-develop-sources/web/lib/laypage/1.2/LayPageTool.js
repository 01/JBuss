
function LayPage(tab,totalPage,totalRow,pageNumber,fn,arr){

    $("#tabPage").remove();
    var div = document.createElement("div");
    $(div).attr("id","tabPage");
    $(div).attr("class","text-r");
    $(div).css("paddingTop","5px");
    $("#"+tab).after($(div));

    laypage({
        cont: 'tabPage', //容器。值支持id名、原生dom对象，jquery对象。
        pages: totalPage, //通过后台拿到的总页数
        count:totalRow,
        curr: pageNumber || 1, //当前页
        groups: 5,//连续显示分页数
        skip: true, //是否开启跳页
        first:'首页',
        last:'尾页',
        //prev:'<',
        //next:'>',
        layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip'],
        jump: function(obj, first){ //触发分页后的回调
            //当前页码／总页码 共xxxx条记录
            if(!first){ //一定要加此判断，否则初始时会无限刷新(不是第一页就会执行)
                pageNumber = obj.curr;
                if(typeof(eval(fn)) == "function") {
                    var Str = "";
                    if (arr != undefined){
                        for (var i=0;i<arr.length;i++){
                            if (arr[i] != undefined){
                                Str += ",'" + arr[i] + "'";
                            }
                        }
                    }
                    eval(fn + "(" + G_pageSize + "," + pageNumber + Str + ")");
                }else{
                    alert("该函数不存在");
                }
            }
            var str = "当前页码" + pageNumber + "／总页码" + totalPage + "&nbsp;&nbsp;共" + totalRow + "条记录";
            $("#tabPage").append(str);
        }
    });
}
