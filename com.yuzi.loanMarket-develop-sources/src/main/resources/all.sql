#namespace("cuFlash")
  #include("sql/popularizeMo/cuFlash.sql")
#end

#namespace("cuGroup")
  #include("sql/popularizeMo/cuGroup.sql")
#end

#namespace("cuGroupItemAsso")
  #include("sql/popularizeMo/cuGroupItemAsso.sql")
#end

#namespace("cuItem")
  #include("sql/popularizeMo/cuItem.sql")
#end

#namespace("cuItemToday")
  #include("sql/popularizeMo/cuItemToday.sql")
#end

#namespace("telCustomer")
  #include("sql/telMo/telCustomer.sql")
#end

#namespace("telCustomerCollection")
  #include("sql/telMo/telCustomerCollection.sql")
#end

#namespace("telCustomerLoginHistory")
  #include("sql/telMo/telCustomerLoginHistory.sql")
#end
