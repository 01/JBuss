#sql("findCuItemTodayByPage")
  SELECT cit.id citId,cit.current,cit.sort,ci.* FROM cu_item_today cit
	inner JOIN cu_item ci ON cit.itemId = ci.id
	WHERE 1 = 1
	#if(itemName != null && itemName != "")
	and ci.Name LIKE CONCAT('%',#para(itemName),'%')
	#end
	#if(sortWay == "pv")
	ORDER BY ci.pv #(ascOrDesc)
	#elseif(sortWay == "uv")
	ORDER BY ci.uv #(ascOrDesc)
	#else
	ORDER BY cit.sort,cit.id #(ascOrDesc)
	#end
#end

#sql("findNotAddCuItemTodayByPage")
  SELECT * FROM cu_item ci
  WHERE ci.id NOT IN (
  SELECT itemId FROM cu_item_today )
  #if(itemName != null && itemName != '')
     and ci.name like CONCAT('%',#para(itemName),'%')
  #end
  ORDER BY ci.id DESC
#end

#sql("findOneCuTodayItem")
SELECT ci.* FROM cu_item_today cit
	LEFT JOIN cu_item ci ON cit.itemId = ci.id
WHERE current != ''
#end

#sql("clearCurrent")
  update cu_item_today set current = ''
#end