#sql("findCuFlashByPage")
SELECT cf.*,ci.name,ci.url FROM cu_flash cf
	LEFT JOIN cu_item ci ON cf.itemId = ci.id
	WHERE ci.onOff = 'on'
  #if(onOff != null && onOff != '')
	    AND cf.onOff = #para(onOff)
	#end
  #if(name != null && name != '')
       and ci.name like CONCAT('%',#para(name),'%')
  #end
	ORDER BY cf.sort ASC ,cf.id DESC
#end

#sql("findCuFlashBysort")
SELECT cf.*,ci.url FROM cu_flash cf
	LEFT JOIN cu_item ci ON cf.itemId = ci.id
	WHERE ci.onOff='on'
	AND cf.onOff ='on'
	ORDER BY sort LIMIT 0,5
#end

#sql("findCuFlashItemNotAddByPage")
SELECT * FROM cu_item ci
WHERE ci.id NOT IN (SELECT itemId FROM cu_flash)
  and ci.onOff = 'on'
  #if(name != null && name != '')
       and ci.name like CONCAT('%',#para(name),'%')
  #end
ORDER BY ci.id DESC
#end

#sql("findCuFlashById")
SELECT cf.*,ci.name FROM (
	SELECT * FROM cu_flash
	WHERE id = #para(id)) cf
	LEFT JOIN cu_item ci ON cf.itemId = ci.id
#end