#sql("findCuItemByPage")
select *
        from cu_item where 1 = 1
        #if(name != null && name != '')
            and name like CONCAT('%',#para(name),'%')
        #end
        order by id desc
#end

#sql("findCuItemNotAddByPage")
     select * from cu_item ci
        WHERE ci.id NOT IN (
        SELECT iid FROM cu_group_item_asso WHERE gid = #para(gid) )
        #if(name != null && name != '')
           and ci.name like CONCAT('%',#para(name),'%')
        #end
        ORDER BY ci.id DESC
#end

#sql("findMobileItemByGroupId")
SELECT ci.*,cgia.sort
       FROM cu_group_item_asso cgia
       LEFT JOIN cu_item ci ON cgia.iid = ci.id
       where 1=1
       #if(onOff != null && onOff != '')
            and ci.onoff = #para(onOff)
       #end
       and cgia.gid = #para(gid)
       order by cgia.sort,ci.id desc
#end

#sql("findManagerItemByGroupId")
    SELECT ci.*,cgia.sort
        FROM cu_group_item_asso cgia
        LEFT JOIN cu_item ci ON cgia.iid = ci.id
        where 1=1
        #if(onOff != null && onOff != '')
            and ci.onoff = #para(onOff)
        #end
        #if(gid != 0)
            and cgia.gid = #para(gid)
        #end
        order by cgia.sort,ci.id desc
#end

#sql("findMyCollection")
  SELECT * FROM
  (SELECT id mid, itemId
    FROM tel_customer_collection
  WHERE cuid = #(cuId)
  ) tcc
  LEFT JOIN (
	SELECT * FROM cu_item WHERE onOff = 'on'
  ) ci
    ON tcc.itemId = ci.id
    order by tcc.mid
#end

