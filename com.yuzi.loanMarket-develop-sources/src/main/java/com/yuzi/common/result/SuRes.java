package com.yuzi.common.result;

public class SuRes extends ComRes {
    private final static Integer defaultCode = 200;

    public static ComRes Res(Object result){
        ComRes comRes = new ComRes(true,200,"成功");
        comRes.setResult(result);
        return comRes;
    }
}
