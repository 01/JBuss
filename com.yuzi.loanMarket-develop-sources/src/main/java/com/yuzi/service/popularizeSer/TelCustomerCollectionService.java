package com.yuzi.service.popularizeSer;

import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.model.telMo.TelCustomer;
import com.yuzi.model.telMo.TelCustomerCollection;
import com.yuzi.model.telMo.TelCustomerLoginHistory;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DateUtils;

import java.util.HashMap;
import java.util.Map;

public class TelCustomerCollectionService extends BaseService {

    private TelCustomerCollection telCustomerCollectionDao;

    public int collectItem(int itemId){
        TelCustomer telCustomer = super.controller.getSessionAttr(Constant.SessionKey.customerKey);
        int cuId = telCustomer.get("id");
        Record record = telCustomerCollectionDao.isExistCollection(itemId,cuId);
        if (record != null){
            return 0;
        }
        TelCustomerCollection telCustomerCollection = new TelCustomerCollection();
        telCustomerCollection.set("itemId",itemId);
        telCustomerCollection.set("cuId",cuId);
        return telCustomerCollectionDao.addTelCustomerCollection(telCustomerCollection);
    }

    public int unCollectItem(int itemId){
        TelCustomer telCustomer = super.controller.getSessionAttr(Constant.SessionKey.customerKey);
        int cuId = telCustomer.get("id");
        return telCustomerCollectionDao.unCollectItem(itemId,cuId);
    }

    public int isCollectItem(int itemId){
        TelCustomer telCustomer = super.controller.getSessionAttr(Constant.SessionKey.customerKey);
        int cuId = telCustomer.get("id");
        Record record = telCustomerCollectionDao.isExistCollection(itemId,cuId);
        if (record == null){
            return 0;
        }else{
            return 1;
        }
    }
}
