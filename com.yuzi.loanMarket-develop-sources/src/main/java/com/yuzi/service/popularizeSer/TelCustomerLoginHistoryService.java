package com.yuzi.service.popularizeSer;

import com.yuzi.common.Constant;
import com.yuzi.model.telMo.TelCustomer;
import com.yuzi.model.telMo.TelCustomerLoginHistory;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DateUtils;
import com.yuzi.utils.MD5;

import java.util.HashMap;
import java.util.Map;

public class TelCustomerLoginHistoryService extends BaseService {

    private TelCustomerLoginHistory telCustomerLoginHistoryDao;
    private TelCustomer telCustomerDao;

    public Map<String,Object> updateLoginTime(TelCustomerLoginHistory tclh){
        Map<String,Object> map = new HashMap<>();
        int id = tclh.getInt("id");
        String now = DateUtils.getCurrentDateTime();
        TelCustomerLoginHistory telCustomerLoginHistory = telCustomerLoginHistoryDao.findById(id);
        String sendCodeTime = telCustomerLoginHistory.get("sendcodetime");

        int seconds = DateUtils.calTimeDiff(sendCodeTime,now);
        //超过5分钟不允许登陆
        if (seconds > 300){
            map.put("flag",false);
            map.put("result","验证码超时（5分钟内有效）");
            map.put("cu",null);
        }else{
            TelCustomer telCustomer = telCustomerDao.isExistTelCustomer(tclh.get("telephone"));
            super.controller.setSessionAttr(Constant.SessionKey.customerKey,telCustomer);
            //登陆成功，修改session，和上一次登陆时间
            String sessionId = MD5.md5(tclh.get("telephone"),now);
            telCustomer.set("sessionId",sessionId);
            String lastLoginTime = telCustomer.get("lastLoginTime");
            telCustomer.set("lastLoginTime",now);
            telCustomer.update();

            tclh.set("logintime", now);
            telCustomerLoginHistoryDao.updateLoginTime(tclh);
            map.put("flag",true);
            map.put("result","成功登陆");
            map.put("cu",telCustomer);
        }
        return map;
    }



}
