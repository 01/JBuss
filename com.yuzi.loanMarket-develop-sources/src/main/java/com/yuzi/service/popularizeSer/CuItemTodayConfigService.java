package com.yuzi.service.popularizeSer;

import com.jfinal.core.JFinal;
import com.yuzi.model.popularizeMo.CuItemToday;
import com.yuzi.model.popularizeMo.CuItemTodayConfig;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.MD5;

import java.util.List;

public class CuItemTodayConfigService extends BaseService {

    public final static CuItemTodayConfigService dao = new CuItemTodayConfigService();

    private CuItemTodayConfig cuItemTodayConfigDao;
    private CuItemToday cuItemTodayDao;

    public boolean updateCuItemTodayConfig(CuItemTodayConfig cuItemTodayConfig){
        String sortWay = cuItemTodayConfig.get("sortway");
        String ascOrDesc = cuItemTodayConfig.get("ascordesc");
        String timeUnit = cuItemTodayConfig.get("timeunit");
        String onOff = cuItemTodayConfig.get("onOff");
        int timeCount = cuItemTodayConfig.getInt("timecount");
        String text = sortWay + ascOrDesc + timeUnit + timeCount;
        String new_md5 = MD5.md5(text,"config");
        cuItemTodayConfig.set("md5",new_md5);
        return cuItemTodayConfig.update();
    }

    public CuItemTodayConfig findCuItemTodayConfig(){
        return cuItemTodayConfigDao.findCuItemTodayConfig();
    }
}
