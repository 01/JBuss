package com.yuzi.service.popularizeSer;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.model.popularizeMo.CuGroupItemAsso;
import com.yuzi.model.popularizeMo.CuItem;
import com.yuzi.model.telMo.TelCustomer;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DataUtils;
import com.yuzi.utils.UploadFileUtils;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

public class CuItemService extends BaseService {

    public final static CuItemService dao = new CuItemService();

    private CuItem cuItemDao;

    private CuGroupItemAsso cuGroupItemAssoDao;

    public List<Integer> findAllCuItemID(){
        return cuItemDao.findAllCuItemID();
    }

    public Page<Record> findCuItemByPage(int pageNumber, int pageSize, String itemName){
        return cuItemDao.findCuItemByPage(pageNumber,pageSize ,itemName);
    }

    public Page<Record> findCuItemNotAddByPage(int pageNumber ,int pageSize,int gid, String itemName){
        return cuItemDao.findCuItemNotAddByPage(pageNumber,pageSize ,gid ,itemName);
    }

    public Page<Record> findManagerItemByGroupId(int pageNumber, int pageSize,int gid,String onOff){
        return cuItemDao.findManagerItemByGroupId(pageNumber,pageSize,gid,onOff);
    }

    public Page<Record> findMobileItemByGroupId(int gid,String onOff,int pageNumber,int pageSize){
        return cuItemDao.findMobileItemByGroupId(gid, onOff, pageNumber, pageSize);
    }

    public CuItem findItemById(int id){
        return cuItemDao.findById(id);
    }

    public boolean addCuItem(CuItem cuItem ,int gid){
        boolean res = Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                int item_id = cuItemDao.addCuItem(cuItem);
                CuGroupItemAsso cuGroupItemAsso = new CuGroupItemAsso();
                cuGroupItemAsso.set("gid",gid);
                cuGroupItemAsso.set("iid",item_id);
                int giid = cuGroupItemAssoDao.addCuGroupItemAsso(cuGroupItemAsso);
                if (item_id>0 && giid>0){
                    return true;
                }else {
                    return false;
                }
            }
        });
        if (res){
            int id = cuItem.getInt("id");
            String logoPath = cuItem.getStr("logo");
            String logoFileName = DataUtils.logoFileName(id,logoPath);
            String logoPath_new = Constant.FileType.logo + File.separator + logoFileName;
            boolean isOk1 = UploadFileUtils.uploadFileByReame(logoPath,logoPath_new);
            System.out.println("=============logoPath_new: "+logoPath_new + ",isOk1 :" + isOk1);
            if (isOk1){
                CuItem cuItem1 = new CuItem();
                cuItem1.set("id",id);
                cuItem1.set("logo",logoPath_new);
                cuItem1.update();
            }
        }
        return res;
    }

    public boolean updateCuItem(CuItem cuItem){
        boolean isOk1 = cuItem.update();
        if (isOk1){
            int id = cuItem.getInt("id");
            String logoPath = cuItem.getStr("logo");
            String logoFileName = DataUtils.logoFileName(id,logoPath);
            String logoPath_new = Constant.FileType.logo + File.separator + logoFileName;
            boolean isOk2 = UploadFileUtils.uploadFileByReame(logoPath,logoPath_new);
            System.out.println("==============logoPath_new: "+logoPath_new + ",isOk2 :" + isOk2);
            if (isOk2){
                CuItem cuItem1 = new CuItem();
                cuItem1.set("id",id);
                cuItem1.set("logo",logoPath_new);
                cuItem1.update();
            }
        }
        return isOk1;
    }

    public boolean updateCuItemOnOff(CuItem cuItem){
        return cuItem.update();
    }

    public int[] updateCuItemByBatch(List<CuItem> cuItemList){
        return cuItemDao.updateCuItemByBatch(cuItemList);
    }

    public boolean deleteCuItemById(int iid){
        boolean res = Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                boolean isOk1 = cuItemDao.deleteById(iid);
                boolean isOk2 = cuGroupItemAssoDao.deleteCuGroupItemAssoByItemId(iid);
                if (isOk1 && isOk2){
                    return true;
                }else {
                    return false;
                }
            }
        });
        return res;
    }

    public Page<Record> findMyCollection(int pageNumber ,int pageSize){
        TelCustomer telCustomer = this.controller.getSessionAttr(Constant.SessionKey.customerKey);
        int cuId = telCustomer.get("id");
        return cuItemDao.findMyCollection(pageNumber,pageSize,cuId);
    }
}
