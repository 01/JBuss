package com.yuzi.service.popularizeSer;

import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.cache.EhCache;
import com.yuzi.model.popularizeMo.CuItemToday;
import com.yuzi.model.popularizeMo.CuItemTodayConfig;
import com.yuzi.model.telMo.TelCustomer;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DataUtils;
import com.yuzi.utils.DateUtils;
import com.yuzi.utils.MD5;
import com.yuzi.utils.UploadFileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

public class CuItemTodayService extends BaseService {

    final public static CuItemTodayService dao = new CuItemTodayService();

    private CuItemToday cuItemTodayDao;

    private CuItemTodayConfig cuItemTodayConfigDao;

    public Page<CuItemToday> findCuTodayItemByPage(int pageNumber,int pageSize,String itemName){
        CuItemTodayConfig cuItemTodayConfig = cuItemTodayConfigDao.findCuItemTodayConfig();
        String sortWay = cuItemTodayConfig.get("sortway");
        String ascOrDesc = cuItemTodayConfig.get("ascordesc");
        return cuItemTodayDao.findCuTodayItemByPage(pageNumber,pageSize,itemName,sortWay,ascOrDesc);
    }

    public Page<CuItemToday> findNotAddCuItemTodayByPage(int pageNumber,int pageSize,String itemName){
        return cuItemTodayDao.findNotAddCuItemTodayByPage(pageNumber,pageSize,itemName);
    }

    public int addCuItemToday(int itemId){
        CuItemToday cuItemToday = new CuItemToday();
        cuItemToday.set("itemId",itemId);
        cuItemToday.set("sort",0);
        cuItemToday.set("current",0);
        return cuItemTodayDao.addCuTodayItem(cuItemToday);
    }

    public boolean deleteItemToday(int id){
        return cuItemTodayDao.deleteById(id);
    }

    public CuItemToday findCuItemTodayById(int id){
        return cuItemTodayDao.findById(id);
    }

    public boolean updateCuItemTodayById(CuItemToday cuItemToday){
        return cuItemToday.update();
    }

    public CuItemToday findOneCuTodayItemWithCache(){
        CuItemToday cuItemToday = cuItemTodayDao.findOneCuTodayItemWithCache();
        if (null == cuItemToday){
            CuItemTodayConfig cuItemTodayConfig = (CuItemTodayConfig)JFinal.me().getServletContext().getAttribute("itemTodayConfig");
            String sortWay = cuItemTodayConfig.get("sortway");
            String ascOrDesc = cuItemTodayConfig.get("ascordesc");

            cuItemToday = cuItemTodayDao.findAllCuTodayItem(sortWay,ascOrDesc).get(0);
        }
        return cuItemToday;
    }

    public CuItemToday findOneCuTodayItem(){
        return cuItemTodayDao.findOneCuTodayItem();
    }


    //根据配置获取显示今日推荐
    public void setTodayItemByConfig(){
        CuItemTodayConfig cuItemTodayConfig = (CuItemTodayConfig)JFinal.me().getServletContext().getAttribute("itemTodayConfig");
        String sortWay = cuItemTodayConfig.get("sortway");
        String ascOrDesc = cuItemTodayConfig.get("ascordesc");
        String timeUnit = cuItemTodayConfig.get("timeunit");
        int timeCount = cuItemTodayConfig.getInt("timecount");
        String onOff = cuItemTodayConfig.get("onoff");
        String md5 = cuItemTodayConfig.get("md5");
        String text = sortWay + ascOrDesc + timeUnit + timeCount;
        boolean isEqual = MD5.verify(text,"config",md5);

        List<CuItemToday> allCuTodayItem = cuItemTodayDao.findAllCuTodayItem(sortWay,ascOrDesc);
        if (allCuTodayItem == null || allCuTodayItem.size() == 0){
            return;
        }
        Integer index = null;
        CuItemToday curCuItemToday = null;
        CuItemToday nextCuItemToday = null;
        for (CuItemToday itemToday:allCuTodayItem){
            //开启页面功能后，这里必定会存在一个符合条件的obj
            if (StringUtils.isNotEmpty(itemToday.get("current"))){
                index = allCuTodayItem.indexOf(itemToday);
                curCuItemToday = itemToday;
                break;
            }
        }

        int nowHour = DateUtils.getHourOfDay();
        int nowDay = DateUtils.getDayOfMonth();
        int fixedTime = timeCount;

        CuItemToday cuItemToday1 = new CuItemToday();
        cuItemTodayDao.clearCurrent();
        if (!isEqual || index == null){
            //如果有改动，则先清除其他对象的current
            cuItemToday1.set("id",allCuTodayItem.get(0).get("citid"));
            if ("hour".equals(timeUnit)){
                cuItemToday1.set("current",nowHour);
            }else if ("day".equals(timeUnit)) {
                cuItemToday1.set("current",nowDay);
            }
            cuItemToday1.update();
            return;

        }
        if ("off".equals(onOff)){
            return;
        }
        if(index == allCuTodayItem.size() -1){
            curCuItemToday = allCuTodayItem.get(index);
            nextCuItemToday = allCuTodayItem.get(0);
        }else{
            nextCuItemToday = allCuTodayItem.get(index+1);
        }

        String oldTimeStr = curCuItemToday.get("current");
        int oldTime = 0;
        if (null != oldTimeStr) {
            oldTime = Integer.valueOf(oldTimeStr);
        }

        cuItemToday1.set("id",nextCuItemToday.get("citid"));
        if ("hour".equals(timeUnit)){
            if ((nowHour - oldTime < 0) || (nowHour - oldTime >= fixedTime)){
                cuItemToday1.set("current",nowHour);
            }else{
                return;
            }

        }else if ("day".equals(timeUnit)){
            if ((nowDay - oldTime < 0) || (nowDay - oldTime >= fixedTime)){
                cuItemToday1.set("current",nowDay);
            }else{
                return;
            }
        }else{
            return;
        }
        cuItemToday1.update();

        //将最新的对象放入缓存
        EhCache ehCache = new EhCache();
        ehCache.remove("itemToday","cuItemToday");
        findOneCuTodayItemWithCache();
    }


}
