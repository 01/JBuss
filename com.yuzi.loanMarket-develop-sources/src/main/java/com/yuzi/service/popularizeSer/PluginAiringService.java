package com.yuzi.service.popularizeSer;

import com.yuzi.model.popularizeMo.PluginAiring;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DataUtils;

public class PluginAiringService extends BaseService {

    private PluginAiring pluginAiringDao;

    public final static PluginAiringService dao = new PluginAiringService();

    public PluginAiring findPluginAiringById(int id){
        return pluginAiringDao.findById(id);
    }

    public void updateShowTextById(int id){
        PluginAiring pluginAiring = findPluginAiringById(id);
        String text = pluginAiring.get("text");
        String random = String.valueOf(DataUtils.randomXX(3));
        int var1 = Integer.parseInt(pluginAiring.get("var1")) + Integer.parseInt(random);
        text = text.replace("${var1}",var1+"");
        pluginAiring.set("var1",var1);
        pluginAiring.set("showText",text);
        pluginAiring.update();
    }

    public PluginAiring findShowTextById(int id){
        PluginAiring pluginAiring = findPluginAiringById(id);
        pluginAiring.remove("text","var1","var2","var3");
        return pluginAiring;
    }
}
