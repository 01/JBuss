package com.yuzi.service.popularizeSer;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.model.popularizeMo.CuFlash;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DataUtils;
import com.yuzi.utils.UploadFileUtils;

import java.io.File;
import java.util.List;

public class CuFlashService extends BaseService {

    private CuFlash cuFlashDao;

    public Page<Record> findCuFlashByPage(int pageNumber, int pageSize, String flashName,String onOff){
        return cuFlashDao.findCuFlashByPage(pageNumber,pageSize ,flashName,onOff);
    }
    public List<CuFlash> getCuFlashBysort(){
        return cuFlashDao.findCuFlashBysort();
    }

    public List<CuFlash> findCuFlashCacheBySort(){
        return cuFlashDao.findCuFlashCacheBySort();
    }

    public Record findFlashById(int id){
        return cuFlashDao.findCuFlashById(id);
    }

    public Page<Record> findCuFlashItemNotAddByPage(int pageNumber,int pageSize,String itemName){
        return cuFlashDao.findCuFlashItemNotAddByPage(pageNumber,pageSize,itemName);
    }

    public boolean addCuFlash(CuFlash cuFlash){
        int id = cuFlashDao.addCuFlash(cuFlash);
        boolean isOk = true;
        if(id>0){
            String photoPath = cuFlash.getStr("photo");
            String photoFileName = DataUtils.photoFileName(id,photoPath);
            String photoPath_new = Constant.FileType.flash + File.separator + photoFileName;
            boolean isOk1 = UploadFileUtils.uploadFileByReame(photoPath,photoPath_new);
            System.out.println("=============logoPath_new: "+photoPath_new + ",isOk1 :" + isOk1);
            if(isOk1) {
                cuFlash.set("id", id);
                cuFlash.set("photo", photoPath_new);
                isOk = cuFlash.update();
            }
        }
        return isOk;
    }

    public boolean updateCuFlash(CuFlash cuFlash){
        boolean isOk1 = cuFlash.update();
        if (isOk1){
            int id = cuFlash.getInt("id");
            String photoPath = cuFlash.getStr("photo");
            String photoFileName = DataUtils.photoFileName(id,photoPath);
            String photoPath_new = Constant.FileType.flash + File.separator + photoFileName;
            boolean isOk2 = UploadFileUtils.uploadFileByReame(photoPath,photoPath_new);
            System.out.println("==============photoPath_new: "+photoPath_new + ",isOk2 :" + isOk2);
            if (isOk2){
                CuFlash cuFlash1 = new CuFlash();
                cuFlash1.set("id",id);
                cuFlash1.set("photo",photoPath_new);
                cuFlash1.update();
            }
        }
        return isOk1;
    }

    public boolean updateCuFlashOnOff(CuFlash cuFlash){
        return cuFlash.update();
    }



    public boolean deleteCuFlashById(int iid){
        boolean isOk = cuFlashDao.deleteById(iid);
        return isOk;
    }
}
