package com.yuzi.service.popularizeSer;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.model.popularizeMo.CuGroup;
import com.yuzi.model.popularizeMo.CuGroupItemAsso;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DealFieldIsNullUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CuGroupService extends BaseService {

    private CuGroup cuGroupDao;
    private CuGroupItemAsso cuGroupItemAssoDao;

    public Page<Record> findCuGroupByPage(int pageNumber, int pageSize, String name){
        return cuGroupDao.findCuGroupByPage(pageNumber, pageSize, name);
    }

    public Map<String, String> addCuGroup(CuGroup cuGroup){
        Map<String,String> map = new HashMap<>();
        if(isExistCuGroup(cuGroup)) {
            map.put("flag", "false");
            map.put("res", "不能添加重复值");
        }else {
            cuGroup=(CuGroup)DealFieldIsNullUtils.NullConvertToString(cuGroup);
            int count = cuGroupDao.addCuGroup(cuGroup);
            if(count>0) {
                map.put("flag", "true");
                map.put("res", "添加成功");
            }else {
                map.put("flag", "false");
                map.put("res", "添加失败");
            }
        }
        return map;
    }

    public boolean addItemToGroup(int gid,int iid){
        CuGroupItemAsso cuGroupItemAsso = new CuGroupItemAsso();
        cuGroupItemAsso.set("gid",gid);
        cuGroupItemAsso.set("iid",iid);
        int affectCount = cuGroupItemAssoDao.addCuGroupItemAsso(cuGroupItemAsso);
        if (affectCount>0){
            return true;
        }else {
            return false;
        }
    }

    public boolean deleteItemFromGroup(int gid, int iid){
        return cuGroupItemAssoDao.deleteCuGroupItemAssoByBothId(gid, iid);
    }

    public boolean isExistCuGroup(CuGroup cuGroup){
        return cuGroupDao.isExistCuGroup(cuGroup.getStr("name"));
    }

    public boolean updateCuGroup(CuGroup cuGroup){
        return cuGroup.update();
    }

    public boolean deleteCuGroupById(int id){
        return cuGroupDao.deleteById(id);
    }

    public CuGroup findCuGroupById(int id){
        return cuGroupDao.findById(id);
    }

    public List<CuGroup> findAllCuGroupByNoDeal(){
        List<CuGroup> cuGroupList = cuGroupDao.findAllCuGroupByNoDeal();
        return cuGroupList;
    }

}
