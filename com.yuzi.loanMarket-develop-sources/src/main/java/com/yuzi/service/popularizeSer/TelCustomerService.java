package com.yuzi.service.popularizeSer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.model.telMo.TelCustomer;
import com.yuzi.model.telMo.TelCustomerLoginHistory;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.*;
import org.apache.commons.lang3.RandomUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TelCustomerService extends BaseService {

    public final static TelCustomerService dao = new TelCustomerService();

    public TelCustomer telCustomerDao;

    private TelCustomerLoginHistory telCustomerLoginHistoryDao;

    final private static Prop prop = PropKit.use("jConfig.properties");

    public Map<String,Object> sendSMSCode(TelCustomer telCustomer){
        Map<String,Object> map = new HashMap<>();
        String date = DateUtils.getCurrentDateTime();
        new Thread(){
            public void run(){
                addTelCustomer(telCustomer,date);
            }
        }.start();

        //当前环境，如果属于测试环境，则不允许发送验证码
        if (prop.getBoolean("sms.isTest")){
            map.put("flag","false");
            map.put("telHid",0);
            map.put("random",0);
            map.put("result","当前属于测试环境，无法进行此操作。");
            return map;
        }

        //生成验证码
        String random = DataUtils.generateRandom(6);
        String content = prop.get("sms.content").replace("$random",random);
        String phone = telCustomer.getStr("telephone");
        TelCustomerLoginHistory telCustomerLoginHistory = new TelCustomerLoginHistory();
        telCustomerLoginHistory.set("telephone",phone);
        telCustomerLoginHistory.set("smscode",content);
        telCustomerLoginHistory.set("sendcodetime",date);
        boolean flag = SendCodeByMine.sendCodeStatus(phone,content);
        if(flag){
            int telHid = telCustomerLoginHistoryDao.addTelCustomerLoginHistory(telCustomerLoginHistory);
            if (telHid > 0){
                map.put("flag","true");
                map.put("telHid",telHid);
                map.put("random",random);
                map.put("result","验证码发送成功");
            }else{
                map.put("flag","false");
                map.put("telHid",telHid);
                map.put("random",0);
                map.put("result","验证码生成失败");
            }
        }else{
            map.put("flag","false");
            map.put("telHid",0);
            map.put("random",0);
            map.put("result","验证码发送失败");
        }
        return map;
    }


    public void addTelCustomer(TelCustomer telCustomer,String date){

        String phone = telCustomer.getStr("telephone");
        TelCustomer telCustomer1 = telCustomerDao.isExistTelCustomer(phone);
        if (telCustomer1 != null){
            return;
        }
        String url = prop.get("numberAssigned") + phone;
        String content = HttpClientUtils.doGet(url);

        JSONObject jsonObject = JSONObject.parseObject(content);
        JSONObject jsonObject1 = (JSONObject)jsonObject.get("response");
        JSONObject jsonObject2 = (JSONObject)jsonObject1.get(phone);

        JSONObject jsonObject3 = (JSONObject)jsonObject2.get("detail");
        JSONArray jsonArray = (JSONArray)jsonObject3.get("area");
        JSONObject jsonObject4 = (JSONObject)jsonArray.get(0);

        String city = jsonObject4.getString("city");
        String province = jsonObject3.getString("province");
        String type = jsonObject3.getString("type");
        String operator = jsonObject3.getString("operator");

        telCustomer.set("province",province);
        telCustomer.set("city",city);
        telCustomer.set("type",type);
        telCustomer.set("carrier",operator);
        telCustomer.set("registertime",date);
        int random = (int)(Math.random() * 16) + 1;
        if (random<10){
            telCustomer.set("userLogo","user_0" + random + ".gif");
        }else{
            telCustomer.set("userLogo","user_" + random + ".gif");
        }
        telCustomerDao.addTelCustomer(telCustomer);
    }

    public boolean isExistSessionOfAPP(String phone, String sessionId){
        TelCustomer telCustomer = telCustomerDao.isExistTelCustomer(phone,sessionId);
        System.out.println("==============");
        System.out.println("telCustoerm:"+ telCustomer);
        if (telCustomer != null){
            String lastLoginTime = telCustomer.get("lastlogintime");
            String nowTime = DateUtils.getCurrentDateTime();
            //一周为过期
            int diff = DateUtils.calTimeDiff(lastLoginTime,nowTime)/3600/24;
            System.out.println("diff:"+diff);
            System.out.println();
            if (diff<7){
                //未过期
                super.controller.setSessionAttr(Constant.SessionKey.customerKey,telCustomer);
                return true;
            }else{
                //过期
                return false;
            }

        }else{
            return false;
        }
    }

    public TelCustomer isExistSessionOfAPP1(String phone, String sessionId){
        TelCustomer telCustomer = telCustomerDao.isExistTelCustomer(phone,sessionId);
        System.out.println("==============");
        System.out.println("telCustoerm:"+ telCustomer);
        if (telCustomer != null){
            String lastLoginTime = telCustomer.get("lastlogintime");
            String nowTime = DateUtils.getCurrentDateTime();
            //一周为过期
            int diff = DateUtils.calTimeDiff(lastLoginTime,nowTime)/3600/24;
            System.out.println("diff:"+diff);
            System.out.println();
            if (diff<7){
                //未过期
                return telCustomer;
            }else{
                //过期
                return null;
            }

        }else{
            return null;
        }
    }

}
