package com.yuzi.service.popularizeSer;

import com.yuzi.model.popularizeMo.CuGroupItemAsso;
import com.yuzi.service.BaseSer.BaseService;

public class CuGIAssoService extends BaseService {

    private CuGroupItemAsso cuGroupItemAssoDao;

    public CuGroupItemAsso findGIByBothId(int gid,int iid){
        CuGroupItemAsso cuGroupItemAsso = cuGroupItemAssoDao.isExistCuGroupItemAsso(gid ,iid);
        if (cuGroupItemAsso != null){
            return cuGroupItemAsso;
        }else{
            return null;
        }
    }

    public boolean updateCuGIById(CuGroupItemAsso cuGroupItemAsso){
        return cuGroupItemAsso.update();
    }

    public boolean deleteCuGIByBothId(int gid,int iid){
        return cuGroupItemAssoDao.deleteCuGroupItemAssoByBothId(gid ,iid);
    }
}
