package com.yuzi.service.task;

import com.jfinal.plugin.cron4j.ITask;
import com.yuzi.service.popularizeSer.CuItemTodayService;

public class ItemTodayTask implements ITask {
    @Override
    public void stop() {

    }

    @Override
    public void run() {
        CuItemTodayService.dao.setTodayItemByConfig();
    }
}
