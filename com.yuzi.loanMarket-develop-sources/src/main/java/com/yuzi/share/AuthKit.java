package com.yuzi.share;
import com.yuzi.common.Constant;
import com.yuzi.service.systemSer.SysButtonService;


public class AuthKit {

    public AuthKit(){

    }

    //判断按钮权限
    public boolean hasPermssion(Integer roleId,String btnCode){
        //排除超级管理员
        if (Constant.SysConfig.sys_role_id == roleId){
            return true;
        }
        return SysButtonService.dao.findSysBtnByBtnCodeAndRoleId(btnCode,roleId);
    }
}
