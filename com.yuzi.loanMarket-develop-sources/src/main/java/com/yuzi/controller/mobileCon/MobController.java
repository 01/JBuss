package com.yuzi.controller.mobileCon;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ComRes;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.interceptor.ExceptionInterceptor;
import com.yuzi.interceptor.MobileSessionInterceptor;
import com.yuzi.model.popularizeMo.*;
import com.yuzi.model.telMo.TelCustomer;
import com.yuzi.model.telMo.TelCustomerLoginHistory;
import com.yuzi.service.popularizeSer.*;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

//({LoginSessionInterceptor.class,SessionInterceptor.class,ExceptionInterceptor.class,SessionInViewInterceptor.class})
/*@Clear
@Before({ExceptionInterceptor.class,SessionInViewInterceptor.class})*/
public class MobController extends BaseController {

    private CuItemService cuItemService;
    private CuFlashService cuFlashService;
    private TelCustomerService telCustomerService;
    private TelCustomerLoginHistoryService telCustomerLoginHistoryService;
    private TelCustomerCollectionService telCustomerCollectionService;
    private PluginAiringService pluginAiringService;
    private CuItemTodayService cuItemTodayService;

    public void index(){
        render(Constant.PagePath.mobPage + "index.html");
    }

    public void showPage(){
        String page = getPara();
        render(Constant.PagePath.mobPage + page + ".html");
    }

    @Before(MobileSessionInterceptor.class)
    public void showPageByIntercept(){
        String page = getPara();
        render(Constant.PagePath.mobPage + page + ".html");
    }

    public void findMobileItemByGroupId(){
        int gid = getParaToInt("gid");
        String onOff = getPara("onOff");
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        Page<Record> cuItemList = cuItemService.findMobileItemByGroupId(gid, onOff, pageNumber, pageSize);
        renderJson(cuItemList);
    }

    public void findTJItemByConf(){
        CuItemToday cuItemToday = cuItemTodayService.findOneCuTodayItemWithCache();
        renderJson(cuItemToday);
    }

    public void findMyCollection(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        Page<Record> myCollection = cuItemService.findMyCollection(pageNumber,pageSize);
        renderJson(myCollection);
    }

    public void showTelLogin(){
        render(Constant.PageUrl.telLogin);
    }


    public void showUserAgreement(){
        render(Constant.PagePath.mobPage + "user-agreement.html");
    }

    @Before(SessionInViewInterceptor.class)
    public void telLogin(){
        TelCustomerLoginHistory tclh = getModel(TelCustomerLoginHistory.class,"tclh");
        Map<String ,Object> map = telCustomerLoginHistoryService.updateLoginTime(tclh);
        boolean flag = (boolean)map.get("flag");
        if (flag){
            renderJson(SuRes.Res(map.get("cu")));
        }else{
            renderJson(ErRes.Res(map.get("cu")));
        }
    }


    public void sendSmsCode(){
        TelCustomer telCustomer = getModel(TelCustomer.class,"");
        Map<String ,Object> map = telCustomerService.sendSMSCode(telCustomer);
        renderJson(map);
    }

    public void loadCuFlashByCache(){
        List<CuFlash> cuFlashlist  = cuFlashService.findCuFlashCacheBySort();
        renderJson(cuFlashlist);
    }

    @Before(MobileSessionInterceptor.class)
    public void showProductDetail(){
        int itemId = getParaToInt(0);

        CuItem cuItem = cuItemService.findItemById(itemId);
        int flag = telCustomerCollectionService.isCollectItem(itemId);
        setAttr("isCollect",flag);
        setAttr("cuItem", cuItem);
        render(Constant.PagePath.mobPage + "product-detail.html");
    }

    public void collectItem(){
        int itemId = getParaToInt("itemId");
        int collectionId = telCustomerCollectionService.collectItem(itemId);
        if (collectionId>0){
            renderJson(SuRes.Res("收藏成功"));
        }else{
            renderJson(ErRes.Res("收藏失败"));
        }
    }

    public void unCollectItem(){
        int itemId = getParaToInt("itemId");
        int collectionId = telCustomerCollectionService.unCollectItem(itemId);
        if (collectionId>0){
            renderJson(SuRes.Res("取消收藏成功"));
        }else{
            renderJson(ErRes.Res("取消收藏失败"));
        }
    }

    public void isExistSessionOfAPP(){
        String phone = getAttr("phone");
        String sessionId = getAttr("sessionId");
        boolean flag = telCustomerService.isExistSessionOfAPP(phone,sessionId);
        if (flag){
            renderJson(SuRes.Res("sessionId 存在"));
        }else{
            renderJson(ErRes.Res("sessionId 不存在"));
        }
    }


    public void findShowTextById(){
        int id = getParaToInt("id");
        PluginAiring pluginAiring = pluginAiringService.findShowTextById(id);
        if (pluginAiring != null){
            renderJson(SuRes.Res(pluginAiring));
        }else{
            renderJson(ErRes.Res("暂无"));
        }
    }
}
