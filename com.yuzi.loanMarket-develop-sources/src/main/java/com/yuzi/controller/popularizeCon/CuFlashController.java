package com.yuzi.controller.popularizeCon;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.popularizeMo.CuFlash;
import com.yuzi.service.popularizeSer.CuFlashService;

import java.util.List;

public class CuFlashController extends BaseController {

    private CuFlashService cuFlashService;

    public void index(){
        render(Constant.PagePath.popPage + "flash-list.html");
    }

    public void loadCuFlashByPage(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String flashName = getPara("flashName");
        String onOff = getPara("onOff");
        Page<Record> cuFlashPage  = cuFlashService.findCuFlashByPage(pageNumber,pageSize,flashName,onOff);
        renderJson(cuFlashPage);
    }

    //预览Flash效果
    public void showCuFlash(){
        render(Constant.PagePath.popPage + "flash-preview.html");
    }

    public void loadCuFlash(){
        List<CuFlash> cuFlashlist  = cuFlashService.getCuFlashBysort();
        renderJson(cuFlashlist);
    }

    public void showCuFlashAdd() {
        render(Constant.PagePath.popPage + "flash-add.html");
    }


    public void showCuFlashEdit() {
        int id = getParaToInt();
        Record cuFlash = cuFlashService.findFlashById(id);
        setAttr("cuflash", cuFlash);
        render(Constant.PagePath.popPage + "flash-edit.html");
    }

    public void showCuFlashItemNotaddPage(){
        render(Constant.PagePath.popPage + "flash-item-notadd-list.html");
    }

    public void findCuFlashItemNotAddByPage(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String itemName = getPara("itemName");
        Page<Record> cuFlashItemNotAdd  = cuFlashService.findCuFlashItemNotAddByPage(pageNumber,pageSize,itemName);
        renderJson(cuFlashItemNotAdd);
    }

    public void addCuFlash(){
        CuFlash cuFlash = getModel(CuFlash.class,"");
        boolean flag = cuFlashService.addCuFlash(cuFlash);
        if (flag){
            renderJson(SuRes.Res("添加成功"));
        }else{
            renderJson(ErRes.Res("添加失败"));
        }
    }

    public void updateCuFlash(){
        CuFlash cuFlash = getModel(CuFlash.class,"");
        boolean flag = cuFlashService.updateCuFlash(cuFlash);
        if (flag){
            renderJson(SuRes.Res("更新成功"));
        }else {
            renderJson(ErRes.Res("更新失败"));
        }
    }
    public void updateCuFlashOnOff(){
        CuFlash cuFlash = getModel(CuFlash.class,"");
        boolean flag = cuFlashService.updateCuFlashOnOff(cuFlash);
        if (flag){
            renderJson(SuRes.Res("更新成功"));
        }else {
            renderJson(ErRes.Res("更新失败"));
        }
    }

    public void delCuFlashById(){
        int id = getParaToInt("id");
        boolean flag = cuFlashService.deleteCuFlashById(id);
        if (flag){
            renderJson(SuRes.Res("删除成功"));
        }else{
            renderJson(ErRes.Res("删除失败"));
        }
    }
}
