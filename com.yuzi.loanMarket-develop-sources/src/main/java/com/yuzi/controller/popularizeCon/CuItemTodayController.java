package com.yuzi.controller.popularizeCon;

import com.jfinal.plugin.activerecord.Page;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.popularizeMo.CuItemToday;
import com.yuzi.service.popularizeSer.CuItemTodayService;

public class CuItemTodayController extends BaseController {
    private CuItemTodayService cuItemTodayService;

    public void index(){
        render(Constant.PagePath.popPage + "item-today-list.html");
    }

    public void findCuItemTodayByPage(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String itemName = getPara("itemName");
        Page<CuItemToday> cuItemTodayPage = cuItemTodayService.findCuTodayItemByPage(pageNumber,pageSize,itemName);
        renderJson(cuItemTodayPage);
    }

    public void findNotAddCuItemTodayByPage(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String itemName = getPara("itemName");
        Page<CuItemToday> cuItemTodayPage = cuItemTodayService.findNotAddCuItemTodayByPage(pageNumber,pageSize,itemName);
        renderJson(cuItemTodayPage);
    }

    public void showCuItemNotAddPage(){
        render(Constant.PagePath.popPage + "item-today-notadd-list.html");
    }

    public void addCuItemToday(){
        int itemId = getParaToInt("itemId");
        int affectCount = cuItemTodayService.addCuItemToday(itemId);
        if (affectCount>0){
            renderJson(SuRes.Res("添加成功"));
        }else{
            renderJson(ErRes.Res("添加失败"));
        }
    }

    public void deleteItemToday(){
        int id = getParaToInt("id");
        boolean isAffect = cuItemTodayService.deleteItemToday(id);
        if (isAffect){
            renderJson(SuRes.Res("删除成功"));
        }else{
            renderJson(ErRes.Res("删除失败"));
        }
    }

    public void showCuItemTodayEdit(){
        int id = getParaToInt(0);
        CuItemToday cuItemToday = cuItemTodayService.findCuItemTodayById(id);
        setAttr("itemToday",cuItemToday);
        render(Constant.PagePath.popPage + "item-today-edit.html");
    }

    public void updateCuItemToday(){
        CuItemToday cuItemToday = getModel(CuItemToday.class,"");
        boolean isAffect = cuItemTodayService.updateCuItemTodayById(cuItemToday);
        if (isAffect){
            renderJson(SuRes.Res("更新成功"));
        }else{
            renderJson(ErRes.Res("更新失败"));
        }
    }
}
