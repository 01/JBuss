package com.yuzi.controller.popularizeCon;

import com.jfinal.core.JFinal;
import com.jfinal.template.Engine;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.popularizeMo.CuItemTodayConfig;
import com.yuzi.service.popularizeSer.CuItemTodayConfigService;

public class CuItemTodayConfigController extends BaseController {

    private CuItemTodayConfigService cuItemTodayConfigService;

    public void updateCuItemTodayConfig(){
        CuItemTodayConfig cuItemTodayConfig = getModel(CuItemTodayConfig.class,"");
        boolean isAffect = cuItemTodayConfigService.updateCuItemTodayConfig(cuItemTodayConfig);
        if (isAffect){
            JFinal.me().getServletContext().setAttribute("itemTodayConfig",cuItemTodayConfig);
            renderJson(SuRes.Res("更新成功"));
        }else{
            renderJson(ErRes.Res("更新失败"));
        }
    }

    public void showItemTodayConfig(){
        //因为只有一个配置
        CuItemTodayConfig cuItemTodayConfig = cuItemTodayConfigService.findCuItemTodayConfig();
        setAttr("itemTodayConfig",cuItemTodayConfig);
        render(Constant.PagePath.popPage + "item-today-config.html");
    }
}
