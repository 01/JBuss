package com.yuzi.controller.popularizeCon;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.popularizeMo.CuGroup;
import com.yuzi.service.popularizeSer.CuGroupService;

import java.util.List;
import java.util.Map;

public class CuGroupController extends BaseController {

    private CuGroupService cuGroupService;

    public void index(){
        render(Constant.PagePath.popPage + "group-list.html");
    }

    public void groupItem(){
        render(Constant.PagePath.popPage + "item-list.html");
    }

    public void loadCuGroupByPage(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String groupName=getPara("groupName");
        Page<Record> cuGroupPage  = cuGroupService.findCuGroupByPage(pageNumber,pageSize,groupName);
        renderJson(cuGroupPage);
    }

    public void addCuGroup(){
        CuGroup cuGroup = getModel(CuGroup.class,"");
        Map<String ,String> map = cuGroupService.addCuGroup(cuGroup);

        if ("true".equals(map.get("flag").toString()))
            renderJson(SuRes.Res(map.get("res").toString()));
        else{
            renderJson(ErRes.Res(map.get("res").toString()));
        }
    }

    public void addItemToGroup(){
        int gid = getParaToInt("gid");
        int iid = getParaToInt("iid");
        boolean flag = cuGroupService.addItemToGroup(gid, iid);
        if (flag){
            renderJson(SuRes.Res("添加成功！"));
        }else {
            renderJson(ErRes.Res("添加失败！"));
        }
    }

    public void deleteItemFromGroup(){
        int gid = getParaToInt("gid");
        int iid = getParaToInt("iid");
        boolean flag = cuGroupService.deleteItemFromGroup(gid, iid);
        if (flag){
            renderJson(SuRes.Res("删除成功！"));
        }else {
            renderJson(ErRes.Res("删除失败！"));
        }
    }

    public void showCuGroupAdd(){
        render(Constant.PagePath.popPage + "group-add.html");
    }

    public void updateCuGroup(){
        CuGroup cuGroup = getModel(CuGroup.class,"");
        boolean flag = cuGroupService.updateCuGroup(cuGroup);
        if (flag){
            renderJson(SuRes.Res("更新成功"));
        }else {
            renderJson(ErRes.Res("更新失败"));
        }
    }

    public void deleteCuGroupById(){
        int id = getParaToInt("id");
        boolean flag = cuGroupService.deleteCuGroupById(id);
        if (flag){
            renderJson(SuRes.Res("删除成功"));
        }else{
            renderJson(ErRes.Res("删除失败"));
        }
    }

    public void showCuGroupEdit(){
        int id = getParaToInt();
        CuGroup cuGroup = cuGroupService.findCuGroupById(id);
        setAttr("cuGroup",cuGroup);
        render(Constant.PagePath.popPage + "group-edit.html");
    }

    public void findAllCuGroupByNoDeal(){
        List<CuGroup> cuGroupList = cuGroupService.findAllCuGroupByNoDeal();
        renderJson(cuGroupList);
    }
}
