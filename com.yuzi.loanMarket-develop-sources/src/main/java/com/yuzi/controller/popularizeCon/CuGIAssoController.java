package com.yuzi.controller.popularizeCon;

import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.popularizeMo.CuGroupItemAsso;
import com.yuzi.service.popularizeSer.CuGIAssoService;

public class CuGIAssoController extends BaseController {

    private CuGIAssoService cuGIAssoService;

    public void updateCuGIById(){
        CuGroupItemAsso cuGroupItemAsso = getModel(CuGroupItemAsso.class ,"");
        boolean flag = cuGIAssoService.updateCuGIById(cuGroupItemAsso);
        if (flag){
            renderJson(SuRes.Res("更新成功"));
        }else {
            renderJson(ErRes.Res("更新失败"));
        }
    }

    public void showCuGIAssoEdit(){
        int gid = getParaToInt(0);
        int iid = getParaToInt(1);
        CuGroupItemAsso cuGroupItemAsso = cuGIAssoService.findGIByBothId(gid, iid);
        setAttr("giAsso",cuGroupItemAsso);
        render(Constant.PagePath.popPage + "giasso-edit.html");
    }

    public void deleteCuGIByBothId(){
        int gid = getParaToInt("gid");
        int iid = getParaToInt("iid");
        boolean flag = cuGIAssoService.deleteCuGIByBothId(gid, iid);
        if (flag){
            renderJson(SuRes.Res("删除成功"));
        }else{
            renderJson(ErRes.Res("删除失败"));
        }
    }
}
