package com.yuzi.controller.popularizeCon;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.popularizeMo.CuGroup;
import com.yuzi.model.popularizeMo.CuItem;
import com.yuzi.service.popularizeSer.CuGroupService;
import com.yuzi.service.popularizeSer.CuItemService;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class CuItemController extends BaseController {

    private CuItemService cuItemService;

    private CuGroupService cuGroupService;

    public void index(){
        render(Constant.PagePath.popPage + "all-item-list.html");
    }

    public void loadCuItemByPage(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String itemName = getPara("itemName");
        Page<Record> cuItemPage  = cuItemService.findCuItemByPage(pageNumber,pageSize,itemName);
        renderJson(cuItemPage);
    }

    public void findManagerItemByGroupId(){
        int gid = getParaToInt("gid");
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        Page<Record> recordList = cuItemService.findManagerItemByGroupId(pageNumber,pageSize,gid, null);
        renderJson(recordList);
    }
/*
    public void findItemByGidAndOnoff(){
        int gid = getParaToInt("gid");
        String onOff = getPara("onOff");
        List<Record> recordList = cuItemService.findManagerItemByGroupId(gid,onOff);
        renderJson(recordList);
    }*/

    public void showCuItem(){
        int gid = getParaToInt();
        CuGroup cuGroup = cuGroupService.findCuGroupById(gid);
        setAttr("cuGroup",cuGroup);
        render(Constant.PagePath.popPage + "item.html");
    }

    public void showCuItemAdd(){
        int gid = getParaToInt();
        setAttr("gid",gid);
        render(Constant.PagePath.popPage + "item-add.html");
    }

    public void showCuItemView(){
        int id = getParaToInt();
        CuItem cuItem = cuItemService.findItemById(id);
        setAttr("cuItem",cuItem);
        render(Constant.PagePath.popPage + "item-view.html");
    }

    public void addCuItem(){
        CuItem cuItem = getModel(CuItem.class,"it");
        int gid = getParaToInt("gid");
        boolean flag = cuItemService.addCuItem(cuItem,gid);
        if (flag){
            renderJson(SuRes.Res("添加成功"));
        }else{
            renderJson(ErRes.Res("添加失败"));
        }
    }

    public void updateCuItem(){
        CuItem cuItem = getModel(CuItem.class,"");
        boolean flag = cuItemService.updateCuItem(cuItem);
        if (flag){
            renderJson(SuRes.Res("更新成功"));
        }else {
            renderJson(ErRes.Res("更新失败"));
        }
    }

    public void updateCuItemOnOff(){
        CuItem cuItem = getModel(CuItem.class,"");
        boolean flag = cuItemService.updateCuItemOnOff(cuItem);
        if (flag){
            renderJson(SuRes.Res("更新成功"));
        }else {
            renderJson(ErRes.Res("更新失败"));
        }
    }

    public void delCuItemById(){
        int iid = getParaToInt("iid");
        boolean flag = cuItemService.deleteCuItemById(iid);
        if (flag){
            renderJson(SuRes.Res("删除成功"));
        }else{
            renderJson(ErRes.Res("删除失败"));
        }
    }

    public void uploadFile(){
        UploadFile uploadFile = getFile("file1",Constant.FileType.temp);
        String fileName = uploadFile.getFileName();
        if (StringUtils.isNotEmpty(fileName)) {
            renderJson(SuRes.Res(Constant.FileType.temp + File.separator + fileName));
        } else {
            renderJson(ErRes.Res("上传失败"));
        }
    }

    public void showCuItemEdit(){
        int id = getParaToInt();
        CuItem cuItem = cuItemService.findItemById(id);
        setAttr("cuItem",cuItem);
        render(Constant.PagePath.popPage + "item-edit.html");
    }

    public void showCuItemNotAddPage(){
        int gid = getParaToInt();
        setAttr("gid",gid);
        render(Constant.PagePath.popPage + "item-notadd-list.html");
    }

    public void showCuItemNotAdd(){
        int gid = getParaToInt("gid");
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String itemName = getPara("itemName");
        Page<Record> cuItemPage  = cuItemService.findCuItemNotAddByPage(pageNumber,pageSize,gid,itemName);
        renderJson(cuItemPage);
    }
}
