package com.yuzi.controller.systemCon;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.systemMo.SysDict;
import com.yuzi.service.systemSer.SysDictService;

import java.util.List;
import java.util.Map;

public class DictController extends BaseController {

    private SysDictService sysDictService;

    public void index(){
        render(Constant.PagePath.sysPage + "dict-list.html");
    }
    //加载页面
    public void loadDictByPage(){
    	String dictNameSele=getPara("dictNameSele");
    	String dictSele=getPara("dictSele");
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        Page<Record> SysDictPage  = sysDictService.findAllSysDict(pageNumber,pageSize,dictNameSele,dictSele);
        renderJson(SysDictPage);
    }
    
    //根据Pcode查找子集
    public void findSysDictByPcode(){
        String pcode = getPara("pcode");
        List<SysDict> sysDictList = sysDictService.findSysDictByPcode(pcode);
        renderJson(sysDictList);
    }
    
    //打开父集字典的添加页面
    public void showDictAdd(){
        render(Constant.PagePath.sysPage + "dict-add.html");
    }
    //打开父集字典的修改页面
    public void showDictEdit(){
    	Integer id = getParaToInt();
    	SysDict dict=sysDictService.findAllSysDictById(id);
    	setAttr("dict",dict);
        render(Constant.PagePath.sysPage + "dict-edit.html");
    }


    
    
    //父集的详情  跳转 跳转至子集的显示
    public void showSubDictList() {
    	String pcode = getPara();
        setAttr("pcode",pcode);
        render(Constant.PagePath.sysPage + "dict-sub-list.html");
    	
    }
    
    
    //打开子集字典的添加页面   需要传父集的pcode
    public void showDictSubAdd(){
    	String pcode = getPara();
    	setAttr("pcode",pcode);
        render(Constant.PagePath.sysPage + "dict-sub-list-add.html");
    }
    
    //打开子集的修改页面   需要传对象
    public void showDictSubEdit() {
    	Integer id = getParaToInt();
    	SysDict dict=sysDictService.findAllSysDictById(id);
    	setAttr("dict",dict);
       render(Constant.PagePath.sysPage + "dict-sub-list-edit.html");
    	 
    }
    

    //父集的字典添加，与子集共用 
    public void addSysDict(){
        SysDict sysDict = getModel(SysDict.class,"");
        Map<String ,String> map = sysDictService.addSysDict(sysDict);
        if ("true".equals(map.get("flag"))){
            renderJson(SuRes.Res(map.get("res")));
        }else{
            renderJson(ErRes.Res(map.get("res")));
        }
    }
    
    
    
    //修改子集或父集
    public void editSysSubDict(){
        SysDict sysDict = getModel(SysDict.class,"sysDict");
        Map<String ,String> map = sysDictService.editSysDict(sysDict);
        if ("true".equals(map.get("flag"))){
            renderJson(SuRes.Res(map.get("res")));
        }else{
            renderJson(ErRes.Res(map.get("res")));
        }
        
    }
    //删除数据字典   子集父集公用一个删除
    public void deleteSysDict(){
        int id = getParaToInt("id");
        Map<String ,String> map = sysDictService.deleteSysDict(id);
        if ("true".equals(map.get("flag").toString())){
            renderJson(SuRes.Res(map.get("msg").toString()));
        }else{
            renderJson(ErRes.Res(map.get("msg").toString()));
        }
    }
}
