package com.yuzi.controller.systemCon;

import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.systemMo.SysMenu;
import com.yuzi.service.systemSer.MenuService;

import java.util.List;
import java.util.Map;

public class MenuController extends BaseController {

    private MenuService menuService;

    public void index(){
        render(Constant.PagePath.sysPage + "system-category.html");
    }

    public void loadAllSysMenu(){
        //加载菜单
        List<Record> allSysMenu =  menuService.findAllMenu(null);
        renderJson(allSysMenu);
    }

    public void findAllParentSysMenu(){
        //获取根目录和一级目录.
        systemCategoryParentMenu();
        render(Constant.PagePath.sysPage + "system-category-add.html");
    }

    public void findSysMenuById(){
        systemCategoryParentMenu();
        int id = getParaToInt();
        SysMenu sysMenu = menuService.findSysMenuById(id);
        setAttr("myself",sysMenu);
        render(Constant.PagePath.sysPage + "system-category-edit.html");
    }

    public void updateMenu(){
        SysMenu sysMenu = getModel(SysMenu.class,"");
        boolean isOk = menuService.updateMenu(sysMenu);
        if (isOk){
            renderJson(SuRes.Res("更新成功"));
        }else{
            renderJson(ErRes.Res("更新失败"));
        }
    }

    public void deleteSysMenu(){
        int id = getParaToInt("id");
        Map<String ,String> map = menuService.deleteSysMenu(id);
        if ("true".equals(map.get("flag").toString())){
            renderJson(SuRes.Res(map.get("msg").toString()));
        }else{
            renderJson(ErRes.Res(map.get("msg").toString()));
        }
    }

    public void systemCategoryParentMenu(){
        //获取根目录和一级目录
        List<SysMenu> rootAndParentMenu = menuService.getRootAndParentMenu();
        setAttr("rootAndParentMenu",rootAndParentMenu);
    }


    public void addMenu(){
        SysMenu sysMenu = getModel(SysMenu.class,"");
        Map<String ,String> map = menuService.addMenu(sysMenu);

        if ("true".equals(map.get("flag").toString()))
            renderJson(SuRes.Res(map.get("res").toString()));
        else{
            renderJson(ErRes.Res(map.get("res").toString()));
        }

    }



    //栏目禁用/启用
    public void updateSysMenuIsHiddenById(){
        int id = getParaToInt("id");
        String ishidden = getPara("ishidden");
        boolean isOk = menuService.updateSysUserStatusById(id,ishidden);
        if (isOk){
            renderJson(SuRes.Res("更新成功"));
        }else{
            renderJson(ErRes.Res("更新失败"));
        }
    }

}
