package com.yuzi.controller.systemCon;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.systemMo.SysRole;
import com.yuzi.service.systemSer.SysRoleService;

import java.util.Map;

public class RoleController extends BaseController {

	private SysRoleService sysRoleService;

	public void index() {
		render(Constant.PagePath.sysPage + "role-list.html");
	}

	public void loadSysRoleByPage() {
		String roleName = getPara("roleName");
		int pageNumber = getParaToInt("pageNumber");
		int pageSize = getParaToInt("pageSize");
		Page<Record> sysRolePage = sysRoleService.findAllSysRole(pageNumber, pageSize, roleName);
		renderJson(sysRolePage);
	}

	public void showRoleAdd() {
		render(Constant.PagePath.sysPage + "role-add.html");
	}

	public void deleteSysRole() {
		int id = getParaToInt("id");
		Map<String, String> map = sysRoleService.deleteSysRole(id);
		if ("true".equals(map.get("flag").toString())) {
			renderJson(SuRes.Res(map.get("msg").toString()));
		} else {
			renderJson(ErRes.Res(map.get("msg").toString()));
		}
	}

	public void addSysRole() {
		SysRole sysRole = getModel(SysRole.class, "");
		Map<String, String> map = sysRoleService.addSysRole(sysRole);

		if ("true".equals(map.get("flag").toString())) {
			renderJson(SuRes.Res(map.get("msg").toString()));
		} else {
			renderJson(ErRes.Res(map.get("msg").toString()));
		}
	}

	public void findSysRoleById() {
		int id = getParaToInt();
		SysRole sysRole = sysRoleService.getSysRoleById(id);
		setAttr("role", sysRole);
		render(Constant.PagePath.sysPage + "role-edit.html");
	}

	public void updateSysRole() {
		SysRole sysRole = getModel(SysRole.class, "");
		boolean isOk = sysRoleService.updateSysRole(sysRole);
		if (isOk) {
			renderJson(SuRes.Res("更新成功"));
		} else {
			renderJson(ErRes.Res("更新失败"));
		}
	}

	// 角色禁用/启用
	public void updateRoleStatusById() {
		int roleId = getParaToInt("roleId");
		if (roleId == Constant.SysConfig.sys_role_id) {
			renderJson(ErRes.Res("不能禁用超级管理员！"));
			return;
		}
		String status = getPara("status");
		boolean isOk = sysRoleService.updateSysRoleStatusById(roleId, status);
		if (isOk) {
			renderJson(SuRes.Res("更新成功"));
		} else {
			renderJson(ErRes.Res("更新失败"));
		}
	}
}
