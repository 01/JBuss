package com.yuzi.controller.systemCon;

import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.systemMo.SysButton;
import com.yuzi.model.systemMo.SysDict;
import com.yuzi.model.systemMo.SysMenu;
import com.yuzi.model.systemMo.SysRoleMenuButton;
import com.yuzi.service.systemSer.MenuService;
import com.yuzi.service.systemSer.SysButtonService;
import com.yuzi.service.systemSer.SysDictService;

import java.util.List;

public class ButtonController extends BaseController {

    //private final Logger logger = Logger.getLogger(ButtonController.class);

    private MenuService menuService;

    private SysButtonService sysButtonService;

    private SysDictService sysDictService;

    public void index(){
        render(Constant.PagePath.sysPage + "button-list.html");
    }

    public void findAllMenuByNoDeal(){
        List<SysMenu> sysMenuList = menuService.findAllMenuByNoDeal();
        renderJson(sysMenuList);
    }

    public void showButton(){
        Integer menuId = getParaToInt();
        SysMenu sysMenu = menuService.findSysMenuById(menuId);
        setAttr("sysMenu",sysMenu);
        render(Constant.PagePath.sysPage + "button.html");
    }

    public void findButtonByMenuId(){
        Integer menuId = getParaToInt("menuId");
        List<SysButton> sysButtons = sysButtonService.findAllButtonByMenuId(menuId);
        renderJson(sysButtons);
    }

    public void showButtonAdd(){
        Integer menuId = getParaToInt();
        List<SysDict> sysBtnList = sysDictService.findSysDictByPcode("button");
        setAttr("sysBtnList",sysBtnList);
        setAttr("menuId",menuId);
        render(Constant.PagePath.sysPage + "button-add.html");
    }

    public void addButton(){
        SysButton sysButton = getModel(SysButton.class,"");
        boolean isOk = sysButtonService.addButton(sysButton);
        if (isOk)
            renderJson(SuRes.Res("添加成功"));
        else{
            renderJson(ErRes.Res("添加失败"));
        }
    }

    public void delButton(){
        SysRoleMenuButton sysRoleMenuButton = getModel(SysRoleMenuButton.class,"");
        boolean isOk = sysButtonService.delButton(sysRoleMenuButton);
        if(isOk) {
            renderJson(SuRes.Res("删除成功"));
        }else{
            renderJson(ErRes.Res("删除失败"));
        }
    }

    public void showButtonEdit(){
        Integer buttonId = getParaToInt();
        setAttr("sysButton",sysButtonService.findButtonByButtonId(buttonId));
        render(Constant.PagePath.sysPage + "button-edit.html");
    }

    public void editButton(){
        SysButton sysButton = getModel(SysButton.class,"");
        boolean isOk = sysButtonService.editButton(sysButton);
        if (isOk)
            renderJson(SuRes.Res("修改成功"));
        else{
            renderJson(ErRes.Res("修改失败"));
        }
    }


    public void updateSysButtonByisHidden(){
        Integer buttonId = getParaToInt("id");
        String isHidden = getPara("isHidden");
        boolean isOk = sysButtonService.updateSysButtonByisHidden(buttonId,isHidden);
        if (isOk){
            renderJson(SuRes.Res("更新成功"));
        }else{
            renderJson(ErRes.Res("更新失败"));
        }
    }
    public void findAllMenuByRoleNoDeal(){
        Integer roleId=getParaToInt("roleId");
        List<SysMenu> sysMenuRoleList = menuService.findAllMenuByRoleNoDeal(roleId);
        renderJson(sysMenuRoleList);
    }
}
