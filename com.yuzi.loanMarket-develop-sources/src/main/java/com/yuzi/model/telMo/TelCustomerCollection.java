package com.yuzi.model.telMo;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yuzi.model.BaseMo.BaseModel;

public class TelCustomerCollection extends BaseModel<TelCustomerCollection> {

    final public static TelCustomerCollection dao = new TelCustomerCollection();

    public int addTelCustomerCollection(TelCustomerCollection telCustomerCollection){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = telCustomerCollection.set("id",maxIndex+1).save();
        if (isSuc)
            return telCustomerCollection.getInt("id");
        else
            return 0;
    }

    public Record isExistCollection(int itemId, int cuId){
        Kv kv = Kv.by("itemId",itemId).set("cuId",cuId);
        SqlPara sqlPara = Db.getSqlPara("telCustomerCollection.isExistCollection",kv);
        return Db.findFirst(sqlPara);
    }

    public int unCollectItem(int itemId, int cuId){
        Kv kv = Kv.by("itemId",itemId).set("cuId",cuId);
        SqlPara sqlPara = Db.getSqlPara("telCustomerCollection.unCollectItem",kv);
        return Db.update(sqlPara);
    }
}
