package com.yuzi.model.telMo;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class TelCustomer extends BaseModel<TelCustomer> {

    public int addTelCustomer(TelCustomer telCustomer){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = telCustomer.set("id",maxIndex+1).save();
        if (isSuc)
            return telCustomer.getInt("id");
        else
            return 0;
    }

    public TelCustomer isExistTelCustomer(String phone){
        String sql = Db.getSql("telCustomer.isExistTelCustomer");
        List<TelCustomer> telCustomers = this.find(sql,phone);
        if (telCustomers != null && telCustomers.size()>0){
            return telCustomers.get(0);
        }else {
            return null;
        }
    }

    public TelCustomer isExistTelCustomer(String phone ,String sessionId){
        Kv kv = Kv.by("phone",phone).set("sessionId",sessionId);
        SqlPara sqlPara = Db.getSqlPara("telCustomer.isExistTelCustomerByPhoneAndSessionId",kv);
        return this.findFirst(sqlPara);
    }
}
