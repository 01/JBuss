package com.yuzi.model.telMo;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yuzi.model.BaseMo.BaseModel;

public class TelCustomerLoginHistory extends BaseModel<TelCustomerLoginHistory> {

    public boolean updateLoginTime(TelCustomerLoginHistory tclh){
        return tclh.update();
    }

    public int addTelCustomerLoginHistory(TelCustomerLoginHistory customerLoginHistory){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = customerLoginHistory.set("id",maxIndex+1).save();
        if (isSuc)
            return customerLoginHistory.getInt("id");
        else
            return 0;
    }

}
