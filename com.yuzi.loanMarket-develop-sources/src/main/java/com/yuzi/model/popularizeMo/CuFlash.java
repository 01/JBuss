package com.yuzi.model.popularizeMo;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yuzi.common.Constant;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class CuFlash extends BaseModel<CuFlash> {

    public Page<Record> findCuFlashByPage(int pageNumber, int pageSize, String name,String onOff){
        Kv cond = Kv.by("name", name).set("onOff",onOff);
        SqlPara sqlPara = Db.getSqlPara("cuFlash.findCuFlashByPage",cond);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public List<CuFlash> findCuFlashBysort(){
        String sql = Db.getSql("cuFlash.findCuFlashBysort");
        return this.find(sql);
    }

    public List<CuFlash> findCuFlashCacheBySort(){
        String sql = Db.getSql("cuFlash.findCuFlashBysort");
        return this.findByCache(Constant.CacheType.flashCache,"flash",sql);
    }

    public Record findCuFlashById(int id){
        Kv kv = Kv.by("id",id);
        SqlPara sqlPara = getSqlPara("cuFlash.findCuFlashById",kv);
        return Db.findFirst(sqlPara);
    }

    public Page<Record> findCuFlashItemNotAddByPage(int pageNumber,int pageSize,String itemName){
        Kv kv = Kv.by("itemName",itemName);
        SqlPara sqlPara = getSqlPara("cuFlash.findCuFlashItemNotAddByPage",kv);
        return Db.paginate(pageNumber,pageSize,sqlPara);
    }

    public int addCuFlash(CuFlash cuFlash){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = cuFlash.set("id",maxIndex+1).save();
        if (isSuc)
            return cuFlash.getInt("id");
        else
            return 0;
    }


}
