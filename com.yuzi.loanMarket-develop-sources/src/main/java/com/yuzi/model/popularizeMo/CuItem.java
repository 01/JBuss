package com.yuzi.model.popularizeMo;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yuzi.model.BaseMo.BaseModel;
import com.yuzi.model.telMo.TelCustomerCollection;

import java.util.List;

public class CuItem extends BaseModel<CuItem> {

    public Page<Record> findCuItemByPage(int pageNumber, int pageSize, String name){
        Kv cond = Kv.by("name", name);
        SqlPara sqlPara = Db.getSqlPara("cuItem.findCuItemByPage",cond);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public Page<Record> findCuItemNotAddByPage(int pageNumber, int pageSize,int gid , String name){
        Kv cond = Kv.by("name", name).set("gid", gid);
        SqlPara sqlPara = Db.getSqlPara("cuItem.findCuItemNotAddByPage",cond);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public Page<Record> findMobileItemByGroupId(int gid ,String onOff,int pageNumber,int pageSize){
        Kv cond = Kv.by("onOff", onOff).set("gid", gid);
        SqlPara sqlPara = Db.getSqlPara("cuItem.findMobileItemByGroupId",cond);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public Page<Record> findManagerItemByGroupId(int pageNumber, int pageSize,int gid ,String onOff){
        Kv cond = Kv.by("onOff", onOff).set("gid", gid);
        SqlPara sqlPara = Db.getSqlPara("cuItem.findManagerItemByGroupId",cond);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public int addCuItem(CuItem cuItem){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = cuItem.set("id",maxIndex+1).save();
        if (isSuc)
            return cuItem.getInt("id");
        else
            return 0;
    }

    public Page<Record> findMyCollection(int pageNumber ,int pageSize,int cuId){
        Kv cond = Kv.by("cuId", cuId);
        SqlPara sqlPara = Db.getSqlPara("cuItem.findMyCollection",cond);
        return Db.paginate(pageNumber,pageSize,sqlPara);
    }

    public List<Integer> findAllCuItemID(){
        return Db.query("select id from cu_item");
    }

    public int[] updateCuItemByBatch(List<CuItem> cuItemList){
        return Db.batchUpdate(cuItemList,50);
    }
}
