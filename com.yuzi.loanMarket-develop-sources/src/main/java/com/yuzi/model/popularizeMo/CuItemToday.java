package com.yuzi.model.popularizeMo;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class CuItemToday extends BaseModel<CuItemToday> {
    public int addCuTodayItem(CuItemToday cuItemToday){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = cuItemToday.set("id",maxIndex+1).save();
        if (isSuc)
            return cuItemToday.getInt("id");
        else
            return 0;
    }

    public Page<CuItemToday> findCuTodayItemByPage(int pageNumber,int pageSize,String itemName,String sortWay ,String ascOrDesc){
        Kv cond = Kv.by("itemName", itemName).set("sortWay",sortWay).set("ascOrDesc",ascOrDesc);
        SqlPara sqlPara = Db.getSqlPara("cuItemToday.findCuItemTodayByPage",cond);
        return this.paginate(pageNumber, pageSize,sqlPara);
    }

    public List<CuItemToday> findAllCuTodayItem(String sortWay , String ascOrDesc){
        Kv cond = Kv.by("itemName", null).set("sortWay",sortWay).set("ascOrDesc",ascOrDesc);
        SqlPara sqlPara = Db.getSqlPara("cuItemToday.findCuItemTodayByPage",cond);
        return this.find(sqlPara);
    }

    public CuItemToday findOneCuTodayItem(){
        SqlPara sqlPara = Db.getSqlPara("cuItemToday.findOneCuTodayItem");
        return this.findFirst(sqlPara.getSql());
    }

    public CuItemToday findOneCuTodayItemWithCache(){
        SqlPara sqlPara = Db.getSqlPara("cuItemToday.findOneCuTodayItem");
        return this.findFirstByCache("itemToday","cuItemToday",sqlPara.getSql());
    }

    public int clearCurrent(){
        SqlPara sqlPara = Db.getSqlPara("cuItemToday.clearCurrent");
        return Db.update(sqlPara);
    }

    public Page<CuItemToday> findNotAddCuItemTodayByPage(int pageNumber ,int pageSize ,String itemName){
        Kv cond = Kv.by("itemName", itemName);
        SqlPara sqlPara = Db.getSqlPara("cuItemToday.findNotAddCuItemTodayByPage",cond);
        return this.paginate(pageNumber, pageSize,sqlPara);
    }
}
