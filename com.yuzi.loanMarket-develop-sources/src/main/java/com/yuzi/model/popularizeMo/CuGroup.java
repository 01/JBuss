package com.yuzi.model.popularizeMo;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class CuGroup extends BaseModel<CuGroup> {

    public Page<Record> findCuGroupByPage(int pageNumber, int pageSize, String name){
        Kv cond = Kv.by("name", name);
        SqlPara sqlPara = Db.getSqlPara("cuGroup.findCuGroupByPage",cond);
        return Db.paginate(pageNumber, pageSize, sqlPara);
    }

    public int addCuGroup(CuGroup cuGroup){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = cuGroup.set("id",maxIndex+1).save();
        if (isSuc)
            return cuGroup.getInt("id");
        else
            return 0;
    }

    public boolean isExistCuGroup(String name){
        String sql = Db.getSql("cuGroup.isExistCuGroup");
        List<CuGroup> cuGroupList = this.find(sql,name);
        if (cuGroupList != null && cuGroupList.size()>0){
            return true;
        }else {
            return false;
        }
    }

    public boolean deleteCuGroupById(int id){
        return this.deleteById(id);
    }

    public List<CuGroup> findAllCuGroupByNoDeal(){
        String sql = Db.getSql("cuGroup.findAllCuGroupByNoDeal");
        return this.find(sql);
    }
}
