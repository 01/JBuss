package com.yuzi.model.popularizeMo;

import com.jfinal.plugin.activerecord.Db;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class CuGroupItemAsso extends BaseModel<CuGroupItemAsso> {

    public int addCuGroupItemAsso(CuGroupItemAsso cuGroupItemAsso){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = cuGroupItemAsso.set("id",maxIndex+1).save();
        if (isSuc) {
            cuGroupItemAsso.set("sort",0);
            return cuGroupItemAsso.getInt("id");
        }else
            return 0;
    }

    public CuGroupItemAsso isExistCuGroupItemAsso(int gid ,int iid){
        String sql =  Db.getSql("cuGroupItemAsso.isExistCuGroupItemAsso");
        List<CuGroupItemAsso> cuGroupItemAssoList = this.find(sql,gid,iid);
        if (cuGroupItemAssoList != null && cuGroupItemAssoList.size()>0){
            return cuGroupItemAssoList.get(0);
        }else {
            return null;
        }
    }

    public boolean deleteCuGroupItemAssoByItemId(int iid){
        String sql = Db.getSql("cuGroupItemAsso.deleteCuGroupItemAssoByItemId");
        int delCount = Db.delete(sql ,iid);
        if (delCount>0){
            return true;
        }else {
            return false;
        }
    }

    public boolean deleteCuGroupItemAssoByBothId(int gid ,int iid){
        String sql = Db.getSql("cuGroupItemAsso.deleteCuGroupItemAssoByBothId");
        int affectCount = Db.delete(sql,gid,iid);
        if (affectCount>0){
            return true;
        }else {
            return false;
        }
    }

}
