package com.yuzi.model.popularizeMo;

import com.yuzi.model.BaseMo.BaseModel;

public class CuItemTodayConfig extends BaseModel<CuItemTodayConfig> {
    //全局配置 竖表
    public CuItemTodayConfig findCuItemTodayConfig(){
        return this.findFirst("select * from cu_item_today_config");
    }
}
