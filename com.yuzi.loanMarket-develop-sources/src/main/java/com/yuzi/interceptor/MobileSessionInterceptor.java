package com.yuzi.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.model.telMo.TelCustomer;
import com.yuzi.service.popularizeSer.TelCustomerService;
import org.apache.commons.lang3.StringUtils;

public class MobileSessionInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        TelCustomer telCustomer = inv.getController().getSessionAttr(Constant.SessionKey.customerKey);
        if (telCustomer != null){
            inv.invoke();
        }else {
            //////////////为ios兼容性问题添加该模块////////////////
            String phone  = inv.getController().getPara("phone");
            String sessionId = inv.getController().getPara("sessionId");
            if (StringUtils.isNotEmpty(phone) && StringUtils.isNotEmpty(sessionId)){
                TelCustomer telCustomer1 = TelCustomerService.dao.isExistSessionOfAPP1(phone,sessionId);
                System.out.println( " telCustomer1:"+telCustomer1);
                if (telCustomer1 != null){
                    inv.getController().setSessionAttr(Constant.SessionKey.customerKey,telCustomer1);
                    inv.invoke();
                }else{
                    inv.getController().render(Constant.PageUrl.telLogin);
                    return;
                }
            }else{
                inv.getController().render(Constant.PageUrl.telLogin);
            }
            //String requestUrl = inv.getController().getRequest().getRequestURI();
            //inv.getController().renderJson(ErRes.Res("redirect"));
            //inv.getController().redirect(Constant.PageUrl.telLogin);
        }
    }
}
