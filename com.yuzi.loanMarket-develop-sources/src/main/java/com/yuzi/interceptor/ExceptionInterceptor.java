package com.yuzi.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.JFinal;
import com.jfinal.log.Log;

/**
 * JFinal异常统一处理
 */
public class ExceptionInterceptor implements Interceptor {

    final static Log log = Log.getLog(ExceptionInterceptor.class);

    @Override
    public void intercept(Invocation invocation) {
        //Controller controller = invocation.getController();
        //HttpServletRequest request = controller.getRequest();

        try {
            invocation.invoke();
        } catch (Exception e) {
            //log 处理
            logWrite(invocation,e);
        }
    }

    private void logWrite(Invocation inv,Exception e){
        //开发模式
        if (JFinal.me().getConstants().getDevMode()){
            e.printStackTrace();
        }
        StringBuilder sb =new StringBuilder("\n---Exception Log Begin---\n");
        sb.append("Controller:").append(inv.getController().getClass().getName()).append("\n");
        sb.append("Method:").append(inv.getMethodName()).append("\n");
        sb.append("Exception Type:").append(e.getClass().getName()).append("\n");
        sb.append("Exception Details:");
        log.error(sb.toString(),e);
    }
}
